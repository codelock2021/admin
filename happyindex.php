<?php
$con = mysqli_connect('localhost', 'root', '', 'image_gallery');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="decoration" content="Balloon decoration surat">
    <meta name="decoration" content="event management surat">
    <meta name="decoration" content="Baby shower decoration surat">
    <meta name="decoration" content="theme decoration surat">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title>Happy Event - Wedding & Event Planner</title>
    <link href="css/bootstrap.min.css" rel=stylesheet>
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/themify-icons.css" rel="stylesheet">
    <link href="modules/slick/slick.css" rel="stylesheet">
    <link href="modules/slick/slick-theme.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
    <link href="modules/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="modules/youtubepopup/YouTubePopUp.css" rel="stylesheet">
    <link href="modules/swiper/swiper.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    
    <script src="js/header-footer.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-144098545-1');
    </script>
    <script>
        
    </script>
</head>

<body>
    <!-- Preloader -->
    <div id="bethany-page-loading" class="bethany-pageloading">
        <div class="bethany-pageloading-inner"> 
            <img src="images/logos.png" class="logo" alt="logo"> 
        </div>
    </div>
    <!-- Navigation -->
    <div w3-include-html="happyheader.php"></div> 
        <script>
            includeHTML();
        </script>
   
       <!-- slider sections -->
   <div class="custom_slider">
    <div class="one-time">
         <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide12.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div>
        <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide5.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div>
        <!--  <div class="slider_banner ">-->
        <!--    <div style="background-image:url(images/slider/slide1.jpg);" class="eventbg" baby_titletitle="event">-->
        <!--        <h2 class="text decor_title"></h2>-->
        <!--    </div>-->
        <!--</div>-->
        
         <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide13.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div>
        
       

        
    </div>
  
    </div>

       <!-- custom Photo Gallery -->
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 title="gallary">Photo Gallery</h2>
                    <hr class="line line-hr-center">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 gallery-item">
                    <a href="images/ballon/about/Screenshot_20210913-191143_Instagram.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img"> <img src="images/ballon/about/Screenshot_20210913-191143_Instagram.jpg" class="img-fluid mx-auto d-block" alt="gallry1" title="decoration"> </div>
                            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 gallery-item">
                    <a href="images/gallery/Screenshot_20210514-162933_Instagram.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img"> <img src="images/gallery/Screenshot_20210514-162933_Instagram.jpg" class="img-fluid mx-auto d-block" alt="gallry2"> </div>
                            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 gallery-item">
                    <a href="images/gallery/3.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img"> <img src="images/gallery/3.jpg" class="img-fluid mx-auto d-block" alt="gallry3"> </div>
                            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 gallery-item">
                    <a href="images/gallery/Screenshot_20211020-091555_Instagram.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img"> <img src="images/gallery/Screenshot_20211020-091555_Instagram.jpg" class="img-fluid mx-auto d-block" alt="gallry4"> </div>
                            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 gallery-item">
                    <a href="images/ballon/about/w1.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img "> <img src="images/ballon/about/w1.jpg" alt="gallry5" class="img-fluid mx-auto d-block"> </div>
                            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 gallery-item">
                    <a href="images/gallery/Screenshot_20211020-195112_Instagram.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img"> <img src="images/gallery/Screenshot_20211020-195112_Instagram.jpg" class="img-fluid mx-auto d-block" alt="gallry6"> </div>
                            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 gallery-item">
                    <a href="images/gallery/Screenshot_20211016-084701_Instagram.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img"> <img src="images/gallery/Screenshot_20211016-084701_Instagram.jpg" class="img-fluid mx-auto d-block" alt="gallry7"> </div>
                            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 gallery-item">
                    <a href="images/gallery/Screenshot_20211021-230538_Instagram.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img"> <img src="images/gallery/Screenshot_20211021-230538_Instagram.jpg" class="img-fluid mx-auto d-block" alt="gallry8"> </div>
                            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 gallery-item">
                    <a href="images/baby/haldi.jpg" title="decoration" class="img-zoom">
                        <div class="gallery-box">
                            <div class="gallery-img"> <img src="images/baby/haldi.jpg" class="img-fluid mx-auto d-block" alt="gallry9"> </div>
                            <div class="gallery-detail text-center cl2"> <i class="ti-heart"></i> </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

       <!-- custom Video Gallery -->
     <section class="video_gallary container">
               <div class="baby_contain">
                <div class="baby_title">
                    <h2 class="title" title="video gallary">Video Gallery</h2>
                    <hr class="line line-hr-center">
                </div>
            </div>
            <div class="custon_videos ind_video">
                    <div class="video_flex">
                            <div class="video50">
                                <div class="evenr" title="video">
                                   <iframe width="530" height="315" src="https://www.youtube.com/embed/7h6Z1zhffzM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>   
                                </div>
                            </div>
                            <div class="video50">
                                    <div class="evenr"  title="video">
                                      <iframe width="530" height="315" src="https://www.youtube.com/embed/osntpgMwIJM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                              <div class="video50">
                                    <div class="evenr"  title="video">
                                     <iframe width="530" height="315" src="https://www.youtube.com/embed/99_kgndge4c" title="YouTube video player video" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                              <div class="video50">
                                    <div class="evenr"  title="video">
                                        <iframe width="530" height="315" src="https://www.youtube.com/embed/Kx93Rl-Wru0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                    </div>
            </div>
    </section>

        <!-- custom Services -->
    <div class="section-padding hp-services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 title="Services">Our Services</h2>
                    <hr class="line line-hr-center">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="services-item">
                        <img class="services-img" src="images/services/22.jpg" alt="wedding">
                            <div class="services-caption">
                                <div class="services-subtitle"><a href="#">Events</a></div>
                                    <div class="services-title"><h4 title="Baby Welcome"><a href="#">Baby Welcome </a></h4></div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services-item">
                        <img src="images/services/babyshowerservice.webp" alt="baby1"  class="services-img" title="baby shower Ceremonies">
                            <div class="services-caption">
                                <div class="services-subtitle"><a href="#">Events</a></div>
                                    <div class="services-title"><h4 title="baby shower Ceremonies"><a href="#">Baby Shower Ceremonies</a></h4></div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services-item">
                        <img class="services-img" src="images/baby/h2.jpg" alt="corporate">
                            <div class="services-caption">
                                <div class="services-subtitle"><a href="#">Events</a></div>
                                    <div class="services-title"><h4 title="Corporate Events"><a href="#">Corporate Events</a></h4></div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services-item">
                        <img class="services-img" src="images/22.jpg" alt="destination">
                            <div class="services-caption">
                                <div class="services-subtitle"><a href="#">Events</a></div>
                                    <div class="services-title" title="Wedding Entry"><a href="#">Wedding Entry</a></div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services-item">
                        <img class="services-img" src="images/services/birthdayplanner.jpg" alt="birthday">
                            <div class="services-caption">
                                <div class="services-subtitle"><a href="#">Events</a></div>
                                    <div class="services-title"><h4 title="Birthday Party Planner"><a href="#">Birthday Party Planner</a></h4></div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services-item">
                        <img class="services-img" src="images/services/e.jpg" alt="HaldiCeremony">
                            <div class="services-caption">
                                <div class="services-subtitle"><a href="#">Events</a></div>
                                    <div class="services-title"><h4 title="Haldi Ceremony"><a href="#">Haldi Ceremony</a></h4></div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Testimonials -->
    <div class="testimonials section-padding bg-1">
        <div class="background bg-fixed bg-img" data-overlay-dark="2" data-background="images/banner/2.jpg"> </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 title="Client Love">Client Reviews</h2>
                    <hr class="line line-hr-center">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="testim">
                        <div class="box">
                            <div class="item">
                                <div class="cont">
                                    <div class="img"> <img src="images/ballon/about/noimg2.jpg" alt="room1" class="event_img"> </div>
                                    <div class="info">
                                        <h5>Pansuriya Krushali</h5> <span>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                                <p>Your company servises is very ammazing and outstandig decoration.
Your service is very fast.
And your decoraters is good and good behaveiour</p>
                            </div>
                        </div>
                        <div class="box">
                            <div class="item">
                                <div class="cont">
                                    <div class="img"> <img src="images/ballon/about/noimg2.jpg" alt="room1" class="event_img"> </div>
                                    <div class="info">
                                        <h5>Prem Prajapati</h5> <span>
                                                 <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                                <p>A week ago, we were gathered to celebrate birthday of my friend's nephew. In short time notice, we find these decorators and in the short time, they managed everything and we celebrated it like bang!!</p>
                            </div>
                        </div>
                        <div class="box">
                            <div class="item">
                                <div class="cont">
                                    <div class="img"><img src="images/ballon/about/noimg2.jpg" alt="room1" class="event_img"> </div>
                                    <div class="info">
                                        <h5>kinjal patel</h5> <span>
                                                 <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                                <p>Thank you so much for your awesome work and make our function more beautiful.Really like it.Best surpise baby entry.
Moreover rates are reasonable too with amazing decoration.I would definity recommend "Balloon Decoration Team" for every type of event.
                                
                                </p>
                            </div>
                        </div>
                        <div class="box">
                            <div class="item">
                                <div class="cont">
                                    <div class="img"> <img src="images/ballon/about/noimg2.jpg" alt="room1" class="event_img"> </div>
                                    <div class="info">
                                        <h5>Hardik Chanchad</h5> <span>
                                             <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                            <i class="fa fa-star cl_star" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                                <p>Just wanted to thank you for all your amazing work. You took care of everything like you promised! The Baby shower decor was absolutely beautiful. I got a number of compliments at the function and I am so glad we picked you!!
Thanks again!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <!-- custom Contact -->
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Contact Us</h2>
                    <hr class="line line-hr-left">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 mb-4">
                            <p>Thank you so much for reaching out to us - we are excited to connect with you! Tell us about you. If you do not receive response within 24 hours, please contact us directly email or phone.</p>
                            <p><b>Phone :</b> 7600464414 / 9714779996</p>
                            <p><b>eMail :</b> happyeventsurat@gmail.com</p>
                            <p><b>Address :</b> Silver Business Point, 21, Green Rd, Uttran, Surat, Gujarat 394105A

</p>
                        </div>
                        <div class="col-md-5 offset-md-1">
                            <h5>We look forward to being in touch!</h5>
                            <form method="post" class="row">
                                <div class="col-md-6">
                                    <input type="text" name="name" id="name" placeholder="Full Name" />
                                </div>
                                <div class="col-md-6">
                                    <input type="email" name="email" id="email" placeholder="Email" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="phone" name="phone" id="phone" placeholder="Phone" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="text" id="text" placeholder="Service Type" required />
                                </div>
                                <div class="col-md-12">
                                    <textarea name="message" id="message" cols="40" rows="4" placeholder="Message"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <button class="bethany-btn2" type="submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- custom Slider -->
   <div class="container">
            <section class="pozo-section-slider pt-130">
                <div class="next-container-center">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="swiper-slide-block">
                                    <div class="swiper-slide-block-img animate-box" data-animate-effect="fadeInLeft" data-swiper-parallax-y="70%">
                                        <a href="project-page.html"> <img src="images/hpimg/hp1.jpg" alt="event2"> </a>
                                    </div>
                                    <div class="swiper-slide-block-text animate-box" data-animate-effect="fadeInRight">
                                        <h2 data-swiper-parallax-x="-60%" class="next-main-title">To Know Us is to Love Us!</h2>
                                        <h3 data-swiper-parallax-x="-50%" class="next-main-subtitle">baby shower & Event Planner</h3>
                                        <p data-swiper-parallax-x="-40%" class="next-paragraph">We would love to meet up and chat about how we can make YOUR DREAM baby happen!</p>  <span data-swiper-parallax-y="60%" class="next-number">1</span>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="swiper-slide-block">
                                    <div class="swiper-slide-block-img" data-swiper-parallax-y="70%">
                                        <a href="project-page-2.html"><img src="images/hpimg/wel2.jpg" alt="anjali"></a>
                                    </div>
                                    <div class="swiper-slide-block-text">
                                        <h2 data-swiper-parallax-x="-60%" class="next-main-title">Your Personal Dream Maker</h2>
                                        <h3 data-swiper-parallax-x="-50%" class="next-main-subtitle">birthday decoration& event Planner</h3>
                                        <p data-swiper-parallax-x="-40%" class="next-paragraph">Wishing you a blessed birthday today, my dear. I hope you cherish this special occasion with all of your loved ones who are so very important to you.

My best wishes to you and may God bless you and your beautiful life.</p>  <span data-swiper-parallax-y="60%" class="next-number animate-box" data-animate-effect="fadeInUp">2</span>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="swiper-slide-block">
                                    <div class="swiper-slide-block-img" data-swiper-parallax-y="70%">
                                        <a href="project-page-2.html"> <img src="images/hpimg/cover-1-3.jpg" alt="slides3"> </a>
                                    </div>
                                    <div class="swiper-slide-block-text">
                                        <h2 data-swiper-parallax-x="-60%" class="next-main-title"> Wedding Planner</h2>
                                        <h3 data-swiper-parallax-x="-50%" class="next-main-subtitle">Wedding & Event Planner</h3>
                                        <p data-swiper-parallax-x="-40%" class="next-paragraph">No matter your dreams - we can assist you in planning your PWE WEDDINGS & EVENTS.</p> <span data-swiper-parallax-y="60%" class="next-number">3</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-next animate-box" data-animate-effect="fadeInRight"> <i class="ti-arrow-right" aria-hidden="true"></i> </div>
                        <div class="swiper-button-prev animate-box" data-animate-effect="fadeInLeft"> <i class="ti-arrow-left" aria-hidden="true"></i> </div>
                    </div>
                </div>
            </section>
    </div>
    <!-- Footer -->
    <div w3-include-html="footer.html"></div> 
        <script>
            footerHTML();
        </script>
    <!-- toTop -->
    <a href="index.html#" class="totop">TOP</a>
    <!-- jQuery -->
   
</body>
 <script src="js/plugins/jquery-3.5.1.min.js"></script>
 <script src="assets/js/img_ajax1.js"></script>
    <script src="js/plugins/bootstrap.min.js"></script>
    <script src="js/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="js/plugins/jquery.isotope.v3.0.2.js"></script>
    <script src="js/plugins/modernizr-2.6.2.min.js"></script>
    <script src="js/plugins/jquery.waypoints.min.js"></script>
    <script src="modules/owl-carousel/owl.carousel.min.js"></script>
    <script src="modules/slick/slick.js"></script>
    <script src="modules/magnific-popup/jquery.magnific-popup.js"></script>
    <script src="modules/masonry/masonry.pkgd.min.js"></script>
    <script src="modules/youtubepopup/YouTubePopUp.js"></script>
    <script src="modules/swiper/swiper.min.js"></script>
    <script src="js/script.js"></script>
</html>
<script>
    $(document).ready(function () {
        console.log("hello");
            set_slider_image();
        });
</script>