<?php

include('smtp/PHPMailerAutoload.php');

class Register {

     public $host = "localhost";
    public $user = "u402017191_image_gallery";
    public $password = "Image_gallery@99";
    public $db = "u402017191_image_gallery";
    var $connect_db;

    function __construct() {
        $this->connect_db = new mysqli($this->host, $this->user, $this->password, $this->db);
        if (mysqli_connect_errno()) {
            printf("Connection failed:", mysqli_connect_error());
            exit();
        }
        return true;
    }

    function insert_data() {
        $id = (isset($_POST['id']) && $_POST['id'] != "") ? $_POST['id'] : '';
        $email = (isset($_POST['email']) && $_POST['email'] != "") ? $_POST['email'] : 'NULL';
        $uname = '';
        $response = array();
        $chk = '';
        if ($id == '') {
            $error_array = array();
            if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
                $src = $_FILES['image']['tmp_name'];
                $dest = $_FILES['image']['name'];
                $imageFileType = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
                if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png") {
                    
                } else {
                    $error_array["image"] = "Please Insert jpg, jpeg and png File!";
                }
                move_uploaded_file($src, "assets/images/" . $dest);
            } else {
                $error_array["image"] = "Image is Require!";
            }

            $uname = $_POST['uname'];
            if (isset($_POST['uname']) && $_POST['uname'] != "") {
                $uname = $_POST['uname'];
            } else {
                $error_array["username"] = "Username is Require!";
            }
            $password = $_POST['password'];
            $number = preg_match('@[0-9]@', $password);
            $uppercase = preg_match('@[A-Z]@', $password);
            $lowercase = preg_match('@[a-z]@', $password);
            $specialChars = preg_match('@[^\w]@', $password);
            if ($password != '') {
                if (strlen($password) < 8) {
                    $error_array["password"] = "Password must be at least 8 characters.!";
                } else if (!$number || !$uppercase || !$lowercase || !$specialChars) {
                    $error_array["password"] = "Password should be Strong.!";
                }
            } else {
                $error_array["password"] = "password is Require!";
            }
            if (isset($_POST['skill']) && $_POST['skill'] != "") {
                $skill = $_POST['skill'];
            } else {
                $error_array["skill"] = "Skill is Require!";
            }
            if (isset($_POST['gender']) && $_POST['gender'] != "") {
                $gender = $_POST['gender'];
            } else {
                $error_array["gender"] = "Gender is Require!";
            }
            if (isset($_POST['email']) && $_POST['email'] != "") {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $error_array["email"] = "Invalid E-mail Address!";
                }
            } else {
                $error_array["email"] = "Email is Require!";
            }
            if (isset($_POST['pnumber']) && $_POST['pnumber'] != "") {
                $pnumber = $_POST['pnumber'];
            } else {
                $error_array["pnumber"] = "Phone Number is Require!";
            }
            if (isset($_POST['books']) && $_POST['books'] != "") {
                $books = $_POST['books'];
                foreach ($books as $chk1) {
                    $chk .= $chk1 . ",";
                }
            } else {
                $error_array["books"] = "Books is Require!";
            }
            if (isset($_POST['message']) && $_POST['message'] != "") {
                $about = $_POST['message'];
            } else {
                $error_array["message"] = "About is Require!";
            }
            if (!empty($error_array)) {
                $response["result"] = "fail";
                $response["data"] = $error_array;
            } else {
                if ($_POST["id"] == '') {
                    $sql = "INSERT INTO register_data (Id, username, password, email, gender, skill, number, books, about, profile) VALUES ('', '$uname', '$password', '$email', $gender, $skill, $pnumber, '$chk', '$about', '$dest')";
                    if ($this->connect_db->query($sql) === TRUE) {
                        $response["result"] = "success";
                        $response["msg"] = "Insert Successfully! ";
                        $email_sent = $this->sendmailer($email, $password, $uname);
                        $response["mail"] = $email_sent;
                    } else {
                        $response["result"] = "fail";
                        $response["msg"] = "Insert Fail! ";
                    }
                }
            }
        } else {
            $clsid = $_POST['id'];
            $clssel = "select * from register_data where id=$clsid";
            $cls = $this->connect_db->query($clssel);
            $row = $cls->fetch_assoc();
            $profile = $row['profile'];
            $uname = $_POST['uname'];
            $email = $_POST['email'];
            $skill = $_POST['skill'];
            $pnumber = $_POST['pnumber'];
            if (isset($_POST['books']) && $_POST['books'] != "") {
                $books = $_POST['books'];
                $uname = $_POST['uname'];
                foreach ($books as $chk1) {
                    $chk .= $chk1 . ",";
                }
            } else {
                $error_array["books"] = "Books is Require!";
            }
            $about = $_POST['message'];
            $dest = $_FILES['image']['name'];
            if ($dest == '') {
                $clsup = "update register_data set username='$uname' ,email='$email' ,skill='$skill', number='$pnumber', books='$chk', about='$about', profile='$profile' where id='$id'";
            } else {
                $clsup = "update register_data set username='$uname' ,email='$email' ,skill='$skill', number='$pnumber', books='$chk', about='$about', profile='$dest' where id='$id'";
            }
            if ($this->connect_db->query($clsup) === TRUE) {
                $response["result"] = "success";
                $response["msg"] = "Update Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Update Fail! ";
            }
        }
        echo json_encode($response);
    }

    function clssignin() {
        session_start();
        $useremail = $_POST['email'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM image_login WHERE email='$useremail' AND  password='$password'";
        $result = $this->connect_db->query($sql);
        $ud = $result->fetch_assoc();
        $total_records = mysqli_num_rows($result);
        if ($total_records > 0) {
            $_SESSION['id'] = $ud['id'];
            $_SESSION['email'] = $ud['email'];
            $_SESSION['password'] = $ud['password'];
            $response["msg"] = "success";
            $response["data"] = "Login Successfully";
        } else {
            $response["msg"] = "fail";
            $response["data"] = "Invalid Username & Password";
        }
        echo json_encode($response);
    }

    function set_image() {
        $response = array();
        $ctgryid = $_POST['value'];
        $i = 1;
        $clsout = '';
        $record_per_page = 12;
        $page = '';
        $output = '';
        if (isset($_POST["page"])) {
            $page = $_POST["page"];
        } else {
            $page = 1;
        }
        $start_from = ($page - 1) * $record_per_page;
        if ($ctgryid == 10) {
            $query = "SELECT * FROM cls_image ORDER BY id DESC LIMIT $start_from, $record_per_page";
        } else {
            $query = "SELECT * FROM cls_image where category='$ctgryid' LIMIT $start_from, $record_per_page";
        }
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $category = $row['category'];
            $id = $row['id'];
            $image = $row['file'];
            $output .= "<div class='clsdels' id='clsdels" . $id . "'>
            <img class='clsprev' src='assets/images/" . $image . "' height='220' width='220' />
            <button class='btn  btn-raised btn-danger waves-effect clsremove' data-id='" . $id . "'>X</button>
            </div>";
        };
        $output_page = "<div class='page_center'>";
        if ($ctgryid == 10) {
            $page_query = "SELECT * FROM cls_image ORDER BY id DESC";
        } else {
            $page_query = "SELECT * FROM cls_image where category='$ctgryid'";
        }
        $page_result = mysqli_query($this->connect_db, $page_query);
        $total_records = mysqli_num_rows($page_result);
        if ($total_records == 0) {
            $response['result'] = "Fail";
            $response['error'] = "Data Not Found";
        } else if ($total_records > $record_per_page) {
            $total_pages = ceil($total_records / $record_per_page);
            for ($i = 1; $i <= $total_pages; $i++) {
                $output_page .= '<li class="page-item clsCls' . $i . '" aria-current="page">
                <span class="page-link pgntn_link" id="' . $i . '">' . $i . '</span>
            </li>';
            }
            $output_page .= "</div>";
            $response["result"] = "success";
            $response["msg"] = "success";
        }
        $response["data"] = $output;
        $response["pagination"] = $output_page;
        echo json_encode($response);
    }

    function clsdelete_data() {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "DELETE  FROM image_data WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail! ";
        }
        echo json_encode($deletemsg);
    }

    function clsimage() {
        $response = array();
        $error_array = array();
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
            $file_name = $_FILES['image']['name'];
        } else {
            $error_array["image"] = "Image is Require!";
        }
        if (isset($_POST['category']) && $_POST['category'] != "") {
            $category = $_POST['category'];
        } else {
            $error_array["category"] = "Category is Require!";
        }
        $count = count($file_name);
        if ($count < 16) {
            
        } else {
            $error_array["size"] = "You Only add 15 number of Image at Time !";
        }
        if (!empty($error_array)) {
            $response["result"] = "fail";
            $response["error"] = $error_array;
        } else {
            foreach ($_FILES['image']["name"] as $key => $value) {
                $src = $_FILES['image']['tmp_name'][$key];
                $dest = $_FILES['image']['name'][$key];
                $imageFileType = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
                if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png" || $imageFileType == "gif" || $imageFileType == "bmp") {
                    $upload = move_uploaded_file($src, "assets/images/" . $dest);
                    $sql = "INSERT INTO image_data (id,file,category) VALUES (' ','$dest',$category)";
                    if ($this->connect_db->query($sql) === TRUE) {
                        $response["result"] = "success";
                        $response["msg"] = "Uploaded Images Successfully! ";
                    } else {
                        $response["result"] = "fail";
                        $response["msg"] = "Upload Image Fail! ";
                    }
                } else {
                    $response["msg"] = "Please Insert jpg, jpeg, gif, bmp and png Files!";
                }
            }
        }
        echo json_encode($response);
    }

    function sendmailer($email, $password) {
        $mail = new PHPMailer();
        $body = "Email : $email" . "<br>" . "Password : $password" . "<br> <a href='https://codelocksolutions.in/vasu/nexa/index.php' target='_blank' style='background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;'>Login</a>";
// 	$mail->SMTPDebug  = 3;
        $subject = "Employee register";
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->Host = "smtp.hostinger.com";
        $mail->Port = 587;
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Username = "tushar@codelocksolutions.in";
        $mail->Password = "Tushar@99";
        $mail->SetFrom("tushar@codelocksolutions.in");
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress($email);
        $mail->SMTPOptions = array('ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => false
        ));
        if (!$mail->Send()) {
            echo $mail->ErrorInfo;
        } else {
            return 'sent';
        }
    }

    function get_data() {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : '';
        $clsselect = "select * from register_data where id=" . $clsid;
        $cls = $this->connect_db->query($clsselect);
        $row = $cls->fetch_assoc();
        echo json_encode($row);
    }

    function delete_data() {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "DELETE  FROM register_data WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail! ";
        }
        echo json_encode($deletemsg);
    }

    function multidelete() {
        $delete = $_POST['id'];
        $deletemsg = array();
        foreach ($delete as $id) {
            $query = "DELETE FROM register_data WHERE id = '" . $id . "'";
            $result = mysqli_query($this->connect_db, $query) or die("Invalid query");
        }
        if (mysqli_affected_rows($this->connect_db) > 0) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Delete Successfully";
            $deletemsg["id"] = $delete;
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail";
        }
        echo json_encode($deletemsg);
    }

    function clsset_data() {
        $clsid = $_POST['id'];
        $output1 = '';
        $clssel = "SELECT * FROM register_data WHERE id=$clsid";
        $result = mysqli_query($this->connect_db, $clssel);
        while ($row = mysqli_fetch_array($result)) {
            $image = $row['profile'];
            $chk = '';
            $id = $row['id'];
            $output = '';
            $books = explode(",", $row["books"]);
            foreach ($books as $chk1) {
                $chk .= $chk1 . ",";
            }
            $output.="<tr id=tr_" . $id . "> 
                <td></td>
                <td>" . $id . "</td>
                <td><img src='assets/images/" . $image . "' height='50' width='50'></td>
                <td>" . $row["username"] . "</td>
                <td>" . $row["email"] . "</td>
                <td>" . $row["number"] . "</td>
                <td>" . $chk . "</td>
                <td>" . $row["about"] . "</td>
                <td>" . $row["Role"] . "</td>
            </tr>";
        }
        $clsselect = "SELECT * FROM employee WHERE r_id = '$clsid'";
        $result = mysqli_query($this->connect_db, $clsselect);
        while ($row = mysqli_fetch_array($result)) {
            $output1.="<tr id=tr_" . $clsid . ">
                    <td>" . $row["create_at"] . "</td>
                    <td>" . $row["datetime"] . "</td>
                        </tr>";
        }
        $response["data"] = $output;
        $response["clsdata"] = $output1;
        echo json_encode($response);
    }

    function set_data() {
        $response = array();
        $i = 1;
        $clsout = '';
        $record_per_page = 5;
        $page = '';
        $output = '';
        if (isset($_POST["page"])) {
            $page = $_POST["page"];
        } else {
            $page = 1;
        }
        $start_from = ($page - 1) * $record_per_page;
        $query = "SELECT * FROM register_data ORDER BY id DESC LIMIT $start_from, $record_per_page";
        if (isset($_POST['value']) && $_POST['value'] != '') {
            $query = 'SELECT * FROM register_data WHERE username LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%" LIMIT ' . $start_from . ',' . $record_per_page;
        }
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $image = $row['profile'];
            $chk = '';
            $id = $row['id'];
            $books = explode(",", $row["books"]);
            foreach ($books as $chk1) {
                $chk .= $chk1 . ",";
            }
            $slNo = $i + $start_from;
            $output.="<tr id=tr_" . $id . ">   
                <td><input type='checkbox' class='muldel' name='checkbox[]' id=" . $id . "></td>
                <td>" . $slNo . "</td>
                <td><img src='assets/images/" . $image . "' height='50' width='50'></td>
                <td>" . $row["username"] . "</td>
                <td class='clsme'>" . $row["email"] . "</td>
                <td class='clsme'>" . $row["number"] . "</td>
                <td class='clsme'>" . $chk . "</td>
                <td class='clsme'>" . $row["about"] . "</td>
                <td class='clsme'>" . $row["Role"] . "</td>
                <td><a class='btn btn-danger clsdelete btn-outline-dark' data-id='" . $id . "'>Delete</a>   
                    <a class='btn btn1 btn-outline-dark' href='reform.php?id=" . $id . "'>Update</a>
                    <a class='btn  btn-raised bg-black waves-effect waves-light clsview' href='index2.php?id=" . $id . "'>View</a>
                </td>
            </tr>";
            $i++;
        };
        $output_page = "<div class='page_center'>";
        $page_query = "SELECT * FROM register_data ORDER BY id DESC";
        $page_result = mysqli_query($this->connect_db, $page_query);
        $total_records = mysqli_num_rows($page_result);
        $total_pages = ceil($total_records / $record_per_page);
        for ($i = 1; $i <= $total_pages; $i++) {
            $output_page .= '<li class="page-item clsClass' . $i . '" aria-current="page">
                <span class="page-link pagination_link" id="' . $i . '">' . $i . '</span>
            </li>';
        }
        $output_page .= "</div>";
        $response["result"] = "success";
        $response["msg"] = "success";
        $response["data"] = $output;
        $response["pagination"] = $output_page;
        echo json_encode($response);
    }

    function signin() {
        session_start();
        $useremail = $_POST['email'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM register_data WHERE email='$useremail' AND  password='$password'";
        $result = $this->connect_db->query($sql);
        $ud = $result->fetch_assoc();
        $total_records = mysqli_num_rows($result);
        if ($total_records > 0) {
            $_SESSION['id'] = $ud['id'];
            $_SESSION['email'] = $ud['email'];
            $_SESSION['password'] = $ud['password'];
            $_SESSION['Role'] = $ud['Role'];
            $response["msg"] = "success";
            $response["data"] = "Login Successfully";
            $response["role"] = $ud['Role'];
        } else {
            $response["msg"] = "fail";
            $response["data"] = "Invalid Username & Password";
        }
        echo json_encode($response);
    }

    function clstimer() {
        session_start();
        $response = array();
        $clsid = $_SESSION['id'];
        $date = date('Y-m-d');
        $select = "SELECT * FROM employee WHERE r_id = '$clsid'  AND date(create_at) = '$date'";
        $result = $this->connect_db->query($select);
        $ud = $result->fetch_assoc();
        $clsud = $ud['datetime'];
        $total_records = mysqli_num_rows($result);
        if ($total_records == 0) {
            $insert = "INSERT INTO employee (userid, r_id, datetime) VALUES ('' ,'$clsid' ,'$clsud')";
            $clsresult = $this->connect_db->query($insert);
            if ($this->connect_db->query($clsresult) == 0) {
                $response["result"] = "success";
                $response["msg"] = "Insert Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Insert Fail! ";
            }
        } else {
            $time = $_POST['date_time'];
            $update = "update employee set datetime='$time' where r_id='$clsid' AND date(create_at) = '$date'";
            $result = $this->connect_db->query($update);
            if ($this->connect_db->query($result) == 0) {
                $response["result"] = "success";
                $response["msg"] = "Update Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Update Fail! ";
            }
        }
        echo json_encode($response);
    }

    function getTime() {
        session_start();
        $response = array();
        if (isset($_SESSION['id'])) {
            $clsid = $_SESSION['id'];
            $date = date('Y-m-d');
            $output = '';
            $clsoutput = '';
            $clstime = '00:00:00';
            $clsimage = "SELECT * FROM register_data WHERE id = '$clsid'";
            $clsimgresult = $this->connect_db->query($clsimage);
            while ($clsimgrow = mysqli_fetch_array($clsimgresult)) {
                $clsoutput.="<a class='bars'><img src='assets/images/" . $clsimgrow['profile'] . "' height='50' width='50'></a>
                        <a class='navbar-brand' href='index.html'>" . $clsimgrow['username'] . "</a>";
            }
            $clsselect = "SELECT * FROM employee WHERE r_id = '$clsid'";
            if (isset($_POST['value']) && $_POST['value'] != '') {
                $clsvalue = $_POST['value'];
                $clsselect = "SELECT * FROM employee WHERE r_id = '$clsid'  AND date(create_at) = '$clsvalue'";
//                $clsselect = 'SELECT * FROM employee WHERE create_at, r_id LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';    
            }
            $clsresult = $this->connect_db->query($clsselect);
            while ($clsrow = mysqli_fetch_array($clsresult)) {
                $clsgetdate = $clsrow["create_at"];
                $clsdt = new DateTime($clsrow["create_at"]);
                $clsdate = $clsdt->format('Y-m-d');
                if ($date == $clsdate) {
                    $clstime = $clsrow['datetime'];
                }
                $output.="<tr id=tr_" . $clsid . ">
                    <td>" . $clsrow["create_at"] . "</td>
                    <td>" . $clsrow["datetime"] . "</td>
                        </tr>";
            }
            $select = "SELECT * FROM employee WHERE r_id = '$clsid'  AND date(create_at) = '$date'";
            $result = $this->connect_db->query($select);
            $data = mysqli_fetch_object($result);
            if (!empty($data)) {
                $response["result"] = "Success";
                $response["msg"] = "Success";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Something went wrong! ";
            }
            $response["data"] = $output;
            $response["time"] = $clstime;
            $response["clsdata"] = $clsoutput;
        } else {
            $response["result"] = "fail";
            $response["msg"] = "Something went wrong! ";
        }
        echo json_encode($response);
    }

}

?>   