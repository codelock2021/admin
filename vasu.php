<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Vasu Kanani - PHP Developer</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link rel= "icon" href = "assets/images/favicon.png" type = "image/x-icon"> 
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700%7CAllura" rel="stylesheet">
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/ionicons.css" rel="stylesheet">
        <link href="assets/css/fluidbox.min.css" rel="stylesheet">
        <link href="assets/css/vasu.css" rel="stylesheet">
        <link href="assets/css/responsive.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container">
                <div class="heading-wrapper">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="info">
                                <i class="icon ion-ios-location-outline"></i>
                                <div class="right-area">
                                    <h5>2020, Silver Business Point</h5>
                                    <h5>Uttran, Surat, Gujarat 394105</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="info">
                                <i class="icon ion-ios-telephone-outline"></i>
                                <div class="right-area">
                                    <h5>7069192892</h5>
                                    <h6>MON-FRI,9:30AM - 7PM</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="info">
                                <i class="icon ion-ios-chatboxes-outline"></i>
                                <div class="right-area">
                                    <h5>vasu.codelock99@gmail.com</h5>
                                    <h6>REPLY IN 24 HOURS</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="intro-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-1 col-lg-2"></div>
                    <div class="col-md-10 col-lg-8">
                        <div class="intro">
                            <div class="profile-img"><img src="assets/images/vasu.jpeg" alt=""></div>
                            <h2><b>Vasu Kanani</b></h2>
                            <!--<h4 class="font-yellow">Co-founder at codelocksolutions</h4>-->
                            <h4 class="font-yellow">Junior PHP Developer</h4>
                            <ul class="information margin-tb-30">
                                <li><b>BORN : </b>January 29, 1996</li>
                                <li><b>EMAIL : </b>vasu.codelock99@gmail.com</li>
                            </ul>
                            <ul class="social-icons">
                                <li><a href="https://www.linkedin.com/in/vasu-kanani-38b91020b/" target="blank"><i class="ion-social-linkedin"></i></a></li>
                                <li><a href="https://www.instagram.com/vasukanani/" target="blank"><i class="ion-social-instagram"></i></a></li>
                                <li><a href="https://www.facebook.com/profile.php?id=100011415409607" target="blank"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="https://twitter.com/kanani_vasu" target="blank"><i class="ion-social-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
<!--        <section class="portfolio-section section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="heading">
                            <h3><b>Portfolio</b></h3>
                            <h6 class="font-lite-black"><b>MY WORK</b></h6>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="portfolioFilter clearfix margin-b-80">
                            <a href="#" data-filter="*" class="current"><b>ALL</b></a>
                            <a href="#" data-filter=".shopify-apps"><b>SHOPIFY APPS</b></a>
                            <a href="#" data-filter=".shopify-stores"><b>SHOPIFY STORES</b></a>
                            <a href="#" data-filter=".graphic-design"><b>WEB DESIGN</b></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portfolioContainer">
                <div class="p-item shopify-apps">
                    <a href="images/app_1_1.png" data-fluidbox>
                        <img src="images/app_1_1.png" alt=""></a>
                </div>
                <div class="p-item shopify-stores graphic-design">
                    <a href="images/store_2_1.jpg" data-fluidbox>
                        <img src="images/store_2_1.jpg" alt=""></a>
                </div>
                <div class="p-item shopify-stores graphic-design">
                    <a href="images/store_2_2.jpg" data-fluidbox>
                        <img src="images/store_2_2.jpg" alt=""></a>
                </div>
                <div class="p-item shopify-stores graphic-design">
                    <a href="images/store_2_3.jpg" data-fluidbox>
                        <img src="images/store_2_3.jpg" alt=""></a>
                </div>
                <div class="p-item shopify-apps">
                    <a href="images/app_1_2.jpg" data-fluidbox>
                        <img src="images/app_1_2.jpg" alt=""></a>
                </div>
                <div class="p-item p-item-2 shopify-apps">
                    <a class="img" href="images/app_1_4.png" data-fluidbox>
                        <img src="images/app_1_4.png" alt=""></a>
                    <a class="img" href="images/app_1_5.jpg" data-fluidbox>
                        <img src="images/app_1_5.jpg" alt=""></a>
                </div>
                <div class="p-item shopify-apps">
                    <a href="images/app_1_3.jpg" data-fluidbox>
                        <img src="images/app_1_3.jpg" alt=""></a>
                </div>
                 <div class="p-item shopify-apps">
                    <a href="images/b2b2.png" data-fluidbox>
                        <img src="images/b2b2.png" alt=""></a>
                </div>
                <div class="p-item shopify-apps">
                    <a href="images/b2b3.png" data-fluidbox>
                        <img src="images/b2b3.png" alt=""></a>
                </div>
                <div class="p-item shopify-apps">
                    <a href="images/b2b4.png" data-fluidbox>
                        <img src="images/b2b4.png" alt=""></a>
                </div>
                <div class="p-item  graphic-design shopify-stores">
                    <a href="images/store_1_1.jpg" data-fluidbox>
                        <img src="images/store_1_1.jpg" alt=""></a>
                </div>
                <div class="p-item shopify-stores">
                    <a href="images/store_1_2.jpg" data-fluidbox>
                        <img src="images/store_1_2.jpg" alt=""></a>
                </div>
                <div class="p-item  shopify-stores">
                    <a href="images/store_3_1.jpg" data-fluidbox>
                        <img src="images/store_3_1.jpg" alt=""></a>
                </div>
                <div class="p-item shopify-stores">
                    <a href="images/store_3_2.jpg" data-fluidbox>
                        <img src="images/store_3_2.jpg" alt=""></a>
                </div>
                <div class="p-item shopify-stores">
                    <a href="images/store_3_3.jpg" data-fluidbox>
                        <img src="images/store_3_3.jpg" alt=""></a>
                </div>
            </div>
        </section>-->

        <section class="about-section section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="heading">
                            <h3><b>About me</b></h3>
                            <h6 class="font-lite-black"><b>PROFESSIONAL PATH</b></h6>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <p class="margin-b-50">Full-time Junior PHP Developer at CodeLock Solutions Surat. <br>
                            <br>
                            I have worked with startups, local businesses, mobile apps, software, fortune 500 companies, and everything in between.
                            <br>
                            Languages, Frameworks, methodologies, and technologies:
                            <br>
                            Shopify | Shopify stores | MongoDB | ReactJS | Wordpress | PHP (Core to Laravel)
                            <br>
                            Good experience with Shopify application integration like Recomatic Related Products,
                            Rebuy Personalization Engine , Ali Reviews ‑ Product Reviews applications.
                            <br>
                            When not doing Development, Marketing, or running projects I am traveling or out in the woods. I also have been a part of 3 successful startups!
                            Choose to work with me for quality, satisfaction, and reliability. If you start working with me, you Will forget about communication issues..</p>

<!--                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="radial-prog-area margin-b-30">
                                    <div class="radial-progress" data-prog-percent=".95">
                                        <div></div>
                                        <h6 class="progress-title">SHOPIFY APPS</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="radial-prog-area margin-b-30">
                                    <div class="radial-progress" data-prog-percent=".80">
                                        <div></div>
                                        <h6 class="progress-title">SHOPIFY STORES</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="radial-prog-area margin-b-30">
                                    <div class="radial-progress" data-prog-percent=".93">
                                        <div></div>
                                        <h6 class="progress-title">PHP</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="radial-prog-area margin-b-30">
                                    <div class="radial-progress" data-prog-percent=".78">
                                        <div></div>
                                        <h6 class="progress-title">WEB DESIGN</h6>
                                    </div>
                                </div>
                            </div>

                        </div>-->
                    </div>
                </div>
            </div>
        </section>

        <section class="experience-section section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="heading">
                            <h3><b>Work Experience</b></h3>
                            <h6 class="font-lite-black"><b>PREVIOUS JOBS</b></h6>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="experience margin-b-50">
                            <h4><b>JUNIOR PHP DEVELOPER</b></h4>
                            <h5 class="font-yellow"><b>CodeLock Solutions</b></h5>
                            <h6 class="margin-t-10">SEPTEMBER 2021 - PRESENT</h6>
                            <p class="font-semi-white margin-tb-30"> I worked as a JUNIOR PHP DEVELOPER at CodeLock Solutions, And I have experience with some Shopify apps and some of the stores.
                                I made some Shopify Stores for the client by their requirements. </p>
                            <ul class="list margin-b-30">
                                <li>All in One Metafields.</li>
                                <li>Under Construction Coming Soon.</li>
                            </ul>
                        </div>
<!--                        <div class="experience margin-b-50">
                            <h4><b>FREELANCER</b></h4>
                            <h5 class="font-yellow"><b>Shopify developer</b></h5>
                            <h6 class="margin-t-10">JULY 2020 - PRESENT</h6>
                            <p class="font-semi-white margin-tb-30">Completed some work as a freelancer for clients.
                                Set some Shopify stores for a client by their requirements and made some Shopify private app for the client. </p>
                            <ul class="list margin-b-30">
                                <li>https://cloveralleystore.myshopify.com/</li>
                                <li>https://www.atticsalt.in/</li>
                                <li>https://pazzion-india.myshopify.com/ (PW : pazzion)</li>
                                <li>https://bombay-carats.myshopify.com/ (PW : bombay)</li>
                                
                            </ul>
                        </div>-->
                    </div>
                </div>
            </div>
        </section>

        <section class="education-section section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="heading">
                            <h3><b>Education</b></h3>
                            <h6 class="font-lite-black"><b>ACADEMIC CAREER</b></h6>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="education-wrapper">
                            <div class="education margin-b-50">
                                <h4 style="text-transform: uppercase;"><b>Bachelors</b></h4>
                                <h5 class="font-yellow" style="text-transform: uppercase;"><b>UTU - Bsc(IT)</b></h5>
                                <h6 class="font-lite-black margin-t-10">GRADUATED IN JULY 2016(3 YEARS)</h6>
                                <!--							<p class="margin-tb-30">Duis non volutpat arcu, eu mollis tellus. Sed finibus aliquam neque sit amet sodales. 
                                                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla maximus pellentes que velit, 
                                                                                        quis consequat nulla effi citur at. Maecenas sed massa tristique.Duis non volutpat arcu, 
                                                                                        eu mollis tellus. Sed finibus aliquam neque sit amet sodales. </p>-->
                            </div>

<!--                            <div class="education margin-b-50">
                                <h4><b>DIPLOMA</b></h4>
                                <h5 class="font-yellow" style="text-transform: uppercase;"><b>government polytechnic bhuj</b></h5>
                                <h6 class="font-lite-black margin-t-10">COMPLETED IN MAY 2015(3 YEARS)</h6>
                                							<p class="margin-tb-30">Duis non volutpat arcu, eu mollis tellus. Sed finibus aliquam neque sit amet sodales. 
                                                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla maximus pellentes que velit, 
                                                                                        quis consequat nulla effi citur at. Maecenas sed massa tristique.Duis non volutpat arcu, 
                                                                                        eu mollis tellus. Sed finibus aliquam neque sit amet sodales. </p>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!--        <section class="counter-section" id="counter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="counter margin-b-30">
                            <h1 class="title"><b><span class="counter-value" data-duration="1400" data-count="8">0</span></b></h1>
                            <h5 class="desc"><b>Project Completed</b></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="counter margin-b-30">
                            <h1 class="title"><b><span class="counter-value" data-duration="700" data-count="3">0</span></b></h1>
                            <h5 class="desc"><b>Satisfied Clients</b></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="counter margin-b-30">
                            <h1 class="title"><b><span class="counter-value" data-duration="2000" data-count="3">0</span></b></h1>
                            <h5 class="desc"><b>Finished Projects</b></h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        
        <footer>
            <p class="copyright">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> Codelocksolutions <i class="ion-heart" aria-hidden="true"></i> Vasu Kanani
            </p>
        </footer>
        
        <script src="assets/js/jquery-3.2.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        <script src="assets/js/isotope.pkgd.min.js"></script>
        <script src="assets/js/jquery.waypoints.min.js"></script>
        <script src="assets/js/progressbar.min.js"></script>
        <script src="assets/js/jquery.fluidbox.min.js"></script>
        <script src="assets/js/scripts.js"></script>
    </body>
</html>