<!doctype html>
<html class="no-js " lang="en">

    <!-- Mirrored from www.wrraptheme.com/templates/nexa/html/chat.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 01 Jan 2020 09:22:01 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

        <title>:: Nexa :: Chat</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Favicon-->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/chatapp.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
    </head>

    <style>
        .theme-orange .user-info{
            display: flex;
            padding-left: 1px;
        }
    </style>

    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">        
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/logo.svg" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>

        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>

        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>

        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#skins">Skins</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat">Chat</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#settings">Setting</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane in active in active slideRight" id="skins">
                    <div class="slim_scroll">
                        <h6>Flat Color</h6>
                        <ul class="choose-skin">                   
                            <li data-theme="purple"><div class="purple"></div><span>Purple</span></li>
                            <li data-theme="blue"><div class="blue"></div><span>Blue</span></li>
                            <li data-theme="cyan"><div class="cyan"></div><span>Cyan</span></li>
                        </ul>                    
                        <h6>Multi Color</h6>
                        <ul class="choose-skin">                        
                            <li data-theme="black"><div class="black"></div><span>Black</span></li>
                            <li data-theme="deep-purple"><div class="deep-purple"></div><span>Deep Purple</span></li>
                            <li data-theme="red"><div class="red"></div><span>Red</span></li>                        
                        </ul>                    
                        <h6>Gradient Color</h6>
                        <ul class="choose-skin">                    
                            <li data-theme="green"><div class="green"></div><span>Green</span> </li>
                            <li data-theme="orange" class="active"><div class="orange"></div><span>Orange</span></li>
                            <li data-theme="blush"><div class="blush"></div><span>Blush</span></li>
                        </ul>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane pullUp" id="chat">
                    <div class="right_chat slim_scroll">
                        <div class="search">
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Search..." required autofocus>
                                </div>
                            </div>
                        </div>
                        <h6>Recent</h6>
                        <ul class="list-unstyled">
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar4.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Sophia</span>
                                            <span class="message">There are many variations of passages of Lorem Ipsum available</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar5.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Grayson</span>
                                            <span class="message">All the Lorem Ipsum generators on the</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="offline">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar2.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Isabella</span>
                                            <span class="message">Contrary to popular belief, Lorem Ipsum</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="me">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar1.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">John</span>
                                            <span class="message">It is a long established fact that a reader</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar3.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Alexander</span>
                                            <span class="message">Richard McClintock, a Latin professor</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>                        
                        </ul>
                        <h6>Contacts</h6>
                        <ul class="list-unstyled">
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar10.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar6.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar7.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar8.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar9.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar5.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar4.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar3.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar2.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar1.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane slideLeft" id="settings">
                    <div class="settings slim_scroll">
                        <p class="text-left">General Settings</p>
                        <ul class="setting-list">
                            <li> <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox" checked>
                                        <span class="lever switch-col-light-blue"></span></label>
                                </div>
                            </li>
                            <li> <span>Email Redirect</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox">
                                        <span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p class="text-left">System Settings</p>
                        <ul class="setting-list">
                            <li> <span>Notifications</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox" checked>
                                        <span class="lever switch-col-green"></span></label>
                                </div>
                            </li>
                            <li> <span>Auto Updates</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox" checked>
                                        <span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p class="text-left">Account Settings</p>
                        <ul class="setting-list">
                            <li><span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li><span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>

        <section class="content chat-app">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="chat">
                        <div class="chat-header clearfix">
                            <img src="assets/images/xs/avatar1.jpg" alt="avatar" />
                            <div class="chat-about">
                                <div class="chat-with">Chat with Vincent Porter</div>
                                <div class="chat-num-messages">already 32 messages</div>
                            </div>                    
                        </div>
                        <div class="chat-history">
                            <ul>
                                <li class="clearfix">
                                    <div class="message-data text-right"> <span class="message-data-time" >10:10 AM, Today</span> &nbsp; &nbsp; <span class="message-data-name" >John</span> <i class="zmdi zmdi-circle me"></i> </div>
                                    <div class="message other-message float-right"> Hi Vincent, how are you? How is the project coming along? </div>
                                </li>
                                <li>
                                    <div class="message-data">
                                        <span class="message-data-name"><i class="zmdi zmdi-circle online"></i> Vincent</span> <span class="message-data-time">10:12 AM, Today</span>
                                    </div>
                                    <div class="message my-message">
                                        <p>Are we meeting today? Project has been already finished and I have results to show you.</p>
                                        <div class="row">
                                            <div class="col-sm-4"><a href="javascript:void(0);"><img src="assets/images/puppy-1.jpg" alt="" class="img-fluid img-thumbnail"></a> </div>
                                            <div class="col-sm-4"><a href="javascript:void(0);"> <img src="assets/images/puppy-2.jpg" alt="" class="img-fluid img-thumbnail"></a> </div>
                                            <div class="col-sm-4"><a href="javascript:void(0);"> <img src="assets/images/puppy-3.jpg" alt="" class="img-fluid img-thumbnail"> </a> </div>
                                        </div>
                                    </div>
                                </li>                        
                                <li>
                                    <div class="message-data"> <span class="message-data-name"><i class="zmdi zmdi-circle online"></i> Vincent</span> <span class="message-data-time">10:31 AM, Today</span> </div>
                                    <i class="zmdi zmdi-circle online"></i> <i class="zmdi zmdi-circle online" style="color: #AED2A6"></i> <i class="zmdi zmdi-circle online" style="color:#DAE9DA"></i> </li>
                            </ul>
                        </div>
                        <div class="chat-message clearfix">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Comments">
                                </div>
                            </div>
                            <button class="btn btn-raised btn-default">Send</button>
                            <a href="javascript:void(0);" class="btn btn-raised btn-warning"><i class="zmdi zmdi-camera"></i></a>
                            <a href="javascript:void(0);" class="btn btn-raised btn-info"><i class="zmdi zmdi-file-text"></i></a>
                        </div>
                    </div>           
                </div>
                <div id="plist" class="people-list">
                    <a href="javascript:void(0);" class="list_btn"><i class="zmdi zmdi-comments"></i></a>
                    <div class="card">
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#people">People</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#groups">Groups</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane slideLeft active" id="people">
                                <ul class="chat-list list-unstyled m-b-0">
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar1.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Vincent Porter</div>
                                            <div class="status"> <i class="zmdi zmdi-circle online"></i> online </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar2.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Aiden Chavez</div>
                                            <div class="status"> <i class="zmdi zmdi-circle offline"></i> left 7 mins ago </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar3.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Mike Thomas</div>
                                            <div class="status"> <i class="zmdi zmdi-circle online"></i> online </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar4.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Erica Hughes</div>
                                            <div class="status"> <i class="zmdi zmdi-circle online"></i> online </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar5.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Ginger Johnston</div>
                                            <div class="status"> <i class="zmdi zmdi-circle online"></i> online </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar6.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Tracy Carpenter</div>
                                            <div class="status"> <i class="zmdi zmdi-circle offline"></i> left 30 mins ago </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar7.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Christian Kelly</div>
                                            <div class="status"> <i class="zmdi zmdi-circle offline"></i> left 10 hours ago </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar3.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Monica Ward</div>
                                            <div class="status"> <i class="zmdi zmdi-circle online"></i> online </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar5.jpg" alt="avatar" />
                                        <div class="about">
                                            <div class="name">Dean Henry</div>
                                            <div class="status"> <i class="zmdi zmdi-circle offline"></i> offline since Oct 28 </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div role="tabpanel" class="tab-pane slideLeft" id="groups">
                                <ul class="chat-list list-unstyled">
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar6.jpg" alt="avatar"/>
                                        <div class="about">
                                            <div class="name">PHP Lead</div>
                                            <div class="status">6 People </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar7.jpg" alt="avatar"/>
                                        <div class="about">
                                            <div class="name">UI UX Designer</div>
                                            <div class="status">10 People </div>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <img src="assets/images/xs/avatar8.jpg" alt="avatar"/>
                                        <div class="about">
                                            <div class="name">TL Groups</div>
                                            <div class="status">2 People </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>
        </section>
        <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

        <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
        <script>
            $(".list_btn").click(function () {
                $("#plist").toggleClass("open");
            });
        </script>
    </body>

    <!-- Mirrored from www.wrraptheme.com/templates/nexa/html/chat.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 01 Jan 2020 09:22:12 GMT -->
</html>