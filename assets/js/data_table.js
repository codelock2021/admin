$(document).ready(function () {
    $('#example').DataTable({
        "processing": true,
        "ajax": "data_function.php",
        "columns": [
            {data: ''},
            {data: ''},
            {data: 'order_num'},
            {data: 'date'},
            {data: 'time'},
            {data: 'client_contact'},
            {data: 'advance_amount'},
            {data: 'remain_amount'},
            {data: 'total_amount'},
            {data: 'customer_image',
                render: function (data) {
                    return '<img src="assets/images/' + data + '" class="avatar" width="50" height="50"/>';
                }
            }
        ],
//        columnDefs: [{
//                orderable: false,
//                className: 'select-checkbox',
//                targets: 0
//            }],
//        select: {
//            style: 'os',
//            selector: 'td:first-child'
//        },
//        order: [[1, 'asc']],
        columnDefs: [{render: createManageBtn, data: null, targets: [1]}]
    });
});
$('#example tbody').on('click', "#del-btn", function () {
    var row = $(this).parents('tr')[0];
    console.log(row);
    //for row data
    console.log(table.row(row).data().id);
});
function createManageBtn() {
    return '<button id="manageBtn" type="button" onclick="myFunc()" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
}
function myFunc() {
    console.log("Button was clicked!!!");
//    var row = $(this).closest('tr').remove();
    var row = $(this).html();
    console.log(row);
}
