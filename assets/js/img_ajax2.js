$(document).ready(function () {
    $(document).on("click", ".save", function (event) {
        event.preventDefault();
        var title = $(".title_input").val();
        var image = $(".image_input").val();
        var descri = $("#basic-example").val();
        if(title == ""){
           var titleError = "Add Title";
           $(".errorMsg").html(titleError);
        }
        if(image == ""){
            var titleError = "Add Image";
           $(".errorImg").html(titleError);
        }
        if(descri == ""){
            var titleError = "Add Description";
           $(".errordes").html(titleError);
        }
        $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"title": title, "method": "apicall", "image": image, "descri": descri},
        success: function (data) {
            var json_data = JSON.parse(data);
            console.log(json_data);
        }
    });
    });
});