//multistep_form and order JS
var filter_array = {};
var filter_cash_array = {};
$(document).ready(function () {
    $(document).on("focus", ".totalAmount", function () {
        var adamount = $(".advanceAmount").val();
        var reamount = $(".remainAmount").val();
        var total = (+adamount) + (+reamount);
        $(".totalAmount").val(total);
        console.log(total);
    });
    
    
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var current = 1;
    var steps = $("fieldset").length;
    setProgressBar(current); //set progress bar at 1

    function nextClick(thisObj) {
        current_fs = $(thisObj).parent();
        next_fs = $(thisObj).parent().next();//Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active"); //show the next fieldset
        next_fs.show(); //hide the current fieldset with style

        current_fs.animate({opacity: 0}, {
            step: function (now) {
// for making fielset appear animation
                opacity = 1 - now;
                current_fs.css({'display': 'none', 'position': 'relative'});
                next_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(++current); //animated progress bar move ahead
    }

    $(".previous").click(function () {
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
//Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
//show the previous fieldset
        previous_fs.show();
//hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now) {
// for making fielset appear animation
                opacity = 1 - now;
                current_fs.css({'display': 'none', 'position': 'relative'});
                previous_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(--current);
    });
    function setProgressBar(curStep) {
        var percent = parseFloat(100 / steps) * curStep; //set a progress bar value to 25/50/75/100 like this
        percent = percent.toFixed();
        $(".progress-bar").css("width", percent + "%")
    }
    $(".submit").click(function () {
        return false;
    });

    $(".export_to_pdf").click(function () {
        $("#leftsidebar").hide();
        $(".navbar").hide();
        $(".block-header").hide();
        setTimeout(function () {
            window.print();
            $("#leftsidebar").show();
            $(".navbar").show();
            $("setTime.block-header").show();
        }, 1000);
    });

    $(document).on("click", ".action-button", function (event) {
        event.preventDefault();
        var thisObj = $(this);
        var form_data = new FormData($('form')[0]);
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (result) {
                var json_data = $.parseJSON(result);
                if (json_data.result === 'fail') {
                    nextClick(thisObj);
                    $(".error_msg").find("ul").html('');
                    $.each(json_data.data, function (key, value) {
                        $(".error_msg").css('display', 'block');
                        $(".error_msg").find("ul").append('<li>' + value + '</li>');
                    });
//                    json_data["data"] !== undefined ? $(".error_msg").append(json_data["data"]) : $(".error_msg").html("");
                } else {
                    $(".accountId").val(json_data["account_id"]);
                    nextClick(thisObj);
                    $('.clsinsert').css("display", "block");
                    $('.clsinsert').html(json_data["msg"]);
                }
            }
        });
    });
    $('.add').click(function () {
        var th = $(this).closest('.wrap').find('.cls_count');
        th.val(+th.val() + 1);
    });
    $('.sub').click(function () {
        var th = $(this).closest('.wrap').find('.cls_count');
        if (th.val() > 1)
            th.val(+th.val() - 1);
    });
    //radio button click show select box
    $('#props_radio_yes').click(function () {
        console.log($(this).val());
        if ($(this).val() === '1') {
            $('.yes_select').show();
        } else if ($(this).val() === '') {
            $('.yes_select').hide();
        }
    });
    //radio button click show input field
    $('#stage_radio_yes').click(function () {
        console.log($(this).val());
        if ($(this).val() === '1') {
            $('.yes_input').show();
        } else if ($(this).val() === '') {
            $('.yes_input').hide();
        }
    });
    //complete button click show tag input and submit button
    $('.complete').click(function () {
        var text = $(this).attr('value');
        if (text === '1') {
            $('.yes_complete_tag').show();
        } else if (text === '') {
            $('.yes_complete_tag').hide();
        }
    });
    //payment done button click show select box
    $('.payment_recive').click(function () {
        var text = $(this).attr('value');
        console.log(text);
        if (text === '3') {
            $('.yes_payment_select').show();
        } else if (text === '') {
            $('.yes_payment_select').hide();
        }
    });

    //oreder listing multistep data show on multistep order
    $(".clsmuldel").click(function () {
        var post_atr = [];
        $(".order_tbody input[type=checkbox]").each(function () {
            if ($(this).is(":checked")) {
                var id = this.id;
                post_atr.push(id);
            }
        });
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"id": post_atr, "method": "multidelete"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                if (json_data["result"] == "fail") {
                    $('.clsdel').html(json_data["msg"]);
                } else {
//                    deleteItem();
                    $.each(json_data["id"], function (i, l) {
                        $("#tr_" + l).remove();
                    });
                    $('.clsdel').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.clsdel').css("display", "none");
                        loasetTimd_data(1);
                    }, 1000);
                }
            }
        });
    });

    $(".promp_btn").click(function () {
        $('.showSweetAlert').css("display", "block");
    });

});


//customer invoice data function call
function customer_order_data() {
    $('table[data-listing="true"]').each(function () {
        var id = $("#clshidden").val();
        invoice_data(id);
        return false;
    });
}
//customer address elipsis ... think
setTimeout(function () {
    $(".customer_address").each(function () {
        text = $(".customer_address").text();
        console.log(text);
        if (text.length > 50) {
            $(this).html(text.substr(0, 20) + '<span class="elipsis">' + text.substr(20) + '</span><a class="elipsis" href="#">...</a>');
        }
    });
    $(".customer_address > a.elipsis").click(function (e) {
        e.preventDefault(); //prevent '#' from being added to the url
        $(this).prev('span.elipsis').fadeToggle(500);
    });
}, 1000);


//customer invoice data
function invoice_data(id) {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": id, "method": "invoice_data"},
        success: function (data) {
            var json_data = $.parseJSON(data);
            console.log(json_data);
            $('.invoice_head_body').html(json_data["customer_data"]);
            $('.customerDetailsHtml').html(json_data["customer_mobile_data"]);
            $('.invoice_body_pro').html(json_data["item_data"]);
        }
    });
}
//multistep order table data function call
function order_details_data() {
    $('table[data-listing="true"]').each(function () {
        load_data(1, '0', 'on_load');
    });
}
//multistep order table data
function load_data(page, value, call_from) {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"page": page, "value": value, "call_from": call_from, "method": "set_data", "filter_data": filter_array},
        success: function (data) {
            var json_data = $.parseJSON(data);
            console.log(json_data);
            $('.order_tbody').html(json_data["data"]);
            $('.pagination_html').html(json_data["pagination"]);
            $(".page-item").removeClass("active");
            $(".clsClass" + page).addClass("active");
        }
    });
}

//pagination
$(document).on('click', '.pagination_link', function () {
    var page = $(this).attr("data-page_number");
    load_data(page, "0", "on_load");
});

//searching on dashboard
$(document).on('keyup', '.clssearch', function () {
    var value = $(this).val();
    filter_array["serach_value"] = value;
    if (this.value.length >= 3 || this.value.length == 0) {
        load_data(1, value, "on_search");
    }
});
//delete data
$(document).on("click", ".clsdelete", function () {
    var clsthis = this;
    var clsid = $(this).data("id");
    var result = confirm("Want to delete?");
    if (result == true) {
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"id": clsid, "method": "delete_data"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
                if (json_data["result"] == "fail") {
                    $('.clsdel').html(json_data["msg"]);
                } else {
                    $(clsthis).closest("tr").remove();
                    $('.clsdel').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.clsdel').css("display", "none");
                    }, 3000);
                }
            }
        });
    } else {
        alert("something went wrong");
    }
});
//get data
function clsupdate(id) {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": id, "method": "get_data"},
        success: function (result) {
            console.log(result);
            var json_data = $.parseJSON(result);
            $('#clshidden').val(json_data["id"]);
            $('#clshidden1').val(json_data["profile"]);
            $('#uname').val(json_data["username"]);
            $('#email').val(json_data["email"]);
            $('.clsskill').val(json_data["skill"]);
            $('#pnumber').val(json_data["number"]);
            $('.clsabout').val(json_data["about"]);
            $('.clsupdate').val(json_data["msg"]);
        }});
}
//select box with image
$(function () {
    // Set
    var main = $('div.mm-dropdown .textfirst');
    var li = $('div.mm-dropdown > ul > li.input-option');
    var inputoption = $("div.mm-dropdown .option");
    var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="20" height="20" class="down" />';

    // Animation
    main.click(function () {
        main.html(default_text);
        li.toggle('fast');
    });

    // Insert Data
    li.click(function () {
        // hide
        li.toggle('fast');
        var livalue = $(this).data('value');
        var lihtml = $(this).html();
        main.html(lihtml);
        inputoption.val(livalue);
    });
});

$(function () {
    // Set
    var main = $('div.mm-dropdown1 .textfirst');
    var li = $('div.mm-dropdown1 > ul > li.input-option');
    var inputoption = $("div.mm-dropdown1 .option");
    var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="20" height="20" class="down" />';

    // Animation
    main.click(function () {
        main.html(default_text);
        li.toggle('fast');
    });

    // Insert Data
    li.click(function () {
        // hide
        li.toggle('fast');
        var livalue = $(this).data('value');
        var lihtml = $(this).html();
        main.html(lihtml);
        inputoption.val(livalue);
    });
});


function set_order_num() {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": "set_order_num"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            console.log(json_data);
            $('.cls_or_num').val(json_data);
        }
    });
}



//login process
$(document).on("click", "#clssignin", function (event) {
    event.preventDefault();
    var email = $('#email').val();
    var password = $('#password').val();
    if (email !== '' && password !== '') {
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'post',
            data: {"email": email, "password": password, "method": "clssignin"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                if (json_data["msg"] == "success") {
                    $('.userName1').html(json_data["data"]);
                    window.location.href = "img_gallery.php";
                } else {
                    $('.userName1').html(json_data["data"]);
                    return false;
                }
            }
        });
    } else {
        $(".userName1").text("Please Enter Email And password !");
    }
});
function set_image() {
    var value = $('.lvpvctgry option:selected').val();
    pagination_image(1, value);
}

$(document).on("click", ".clsremove", function (event) {
    event.preventDefault();
    var clsthis = this;
    var clsid = $(this).data("id");
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": clsid, "method": "clsdelete_data"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            console.log(json_data);
            if (json_data["result"] == "Fail") {
                $('.clsdel').html(json_data['error']);
            } else {
                $(clsthis).closest('#clsdels' + clsid).remove();
                $('.clsdel').html(json_data["msg"]);
                setTimeout(function () {
                    $('.clsdel').css("display", "none");
                }, 3000);
                set_image();
            }
        }
    });
});

$(document).on("change", ".lvpvctgry", function (event) {
    event.preventDefault();
    var value = $('.lvpvctgry option:selected').val();
    set_image();
    pagination_image(1, value);
});

//multistep order page dropdown data call
$(document).on("change", ".bootstrap-select.show_order_data_filter", function (event) {
    event.preventDefault();
    var status = $('.show_order_data_filter option:selected').val();
    filter_array["status_value"] = status;
    load_data(1, status, 'on_status_change');
});

//Filter By Date
$(document).on("change", '#filter_date', function () {
    var date = new Date($('#filter_date').val());
    var date_value = $("#filter_date").val();
    filter_array["date_value"] = date_value;
    load_data(1, date_value, "on_date_filter");
});

//multistep order page dropdown update call
$(document).on("change", ".show_order_status", function () {
    var thisObj = this;
    var value = $(this).find('option:selected').val();
    var id = $(this).find('option:selected').attr("data-id");
    var clsclass = $(this).find('option:selected').attr("class");
    console.log(clsclass);
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"dropdown": value, "id": id, "class": clsclass, "method": "set_order_status"},
        success: function (result) {
//            console.log(result);
            var json_data = $.parseJSON(result);
            if (json_data["result"] == "Fail") {
                $('.cls_order_msg').html(json_data['msg']);
            } else {
                if (json_data["class"] == "panding_color") {
                    $(thisObj).css("background-color", "yellow");
                } else if (json_data["class"] == "complete_color") {
                    $(thisObj).css("background-color", "#b3e5b3");
                } else if (json_data["class"] == "cancel_color") {
                    $(thisObj).css("background-color", "red");
                } else if (json_data["class"] == "done_color") {
                    $(thisObj).css("background-color", "green");
                }
//                $(thisObj).closest("tr").find(".show_order_status").addClass(json_data["class"]);
                $('.cls_order_msg').html(json_data["msg"]);
                setTimeout(function () {
                    $('.cls_order_msg').css("display", "none");
                }, 3000);
            }
        }
    });
});



function pagination_image(page) {
    var value = $('.lvpvctgry option:selected').val();
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"value": value, "page": page, "method": "set_image"},
        success: function (data) {
            var json_data = $.parseJSON(data);
            console.log(json_data["error"]);
            if (json_data["result"] == "Fail") {
                $('.clspreview').html("");
                $('.pagination_html').html("");
                $('.clsdata').html(json_data["error"]);
            } else {
                $('.clsdata').html("");
                $('.clspreview').html(json_data["data"]);
                $('.pagination_html').html(json_data["pagination"]);
                $(".page-item").removeClass("active");
                $(".clsCls" + page).addClass("active");
            }
        }
    });
}

$(document).on('click', '.pgntn_link', function () {
    var page = $(this).attr("id");
    pagination_image(page);
});


function upload() {
    $('#fileupload').click();
}

$(function () {
    $("#fileupload").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#dvPreview");
            var i = 1;
            var clstag = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                if (i > 10) {
                    $("#dvPreview").addClass("scrollbar");
                }
                var file = $(this);
                if (clstag.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var div = $("<div class='clsdiv'>");
                        var img = $("<img class='clsimg' />");
                        img.attr("src", e.target.result);
                        var btn = $("<button class='btn  btn-raised btn-danger waves-effect remove'>X</button>");
                        div.attr("</div>");
                        div.append(img);
                        div.append(btn);
                        dvPreview.append(div);
                        $(".remove").click(function () {
                            var clsthis = this;
                            $(clsthis).closest(".clsdiv").remove();
                        });
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    setTimeout(function () {
                        $('.clsfile').css("display", "none");
                    }, 2000);
                    return false;
                }
                i++;
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
    $(".clssubmit").click(function (event) {
        event.preventDefault();
        $(".clsicon").css("display", "inherit");
        var form_data = new FormData($('form')[0]);
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
                $('.clsname').html("");
                $('.clscate').html("");
                $('.clssize').html("");
                if (json_data["result"] == "fail") {
                    $('.clsname').html(json_data["error"]["image"]);
                    $('.clscate').html(json_data["error"]["category"]);
                    $('.clssize').html(json_data["error"]["size"]);
                    $('#clsicon').css("display", "none");
                } else {
                    set_image();
                    $('.fileupload1').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.fileupload1').css("display", "none");
                        $('#clsicon').css("display", "none");
                        $('#dvPreview').css("display", "none");
                    }, 2000);
                }
            }
        });
    });
});

//step-3 next button click send to invoice page
$(document).on('click', '.invoiceBtn', function () {
    var id = $(".accountId").val();
    window.location = 'invoice_data.php?id=' + id;
});



//cashbook page js

//cashbook page insert data
function clscashbook_insertdata() {
    $(".cashClick").click(function (event) {
        event.preventDefault();
        var cash_status = $(this).attr("data-value");
        if (cash_status == '0') {
            cash_status = 'Cash In';
            var classs_name = "cash_in_entry";
        } else {
            cash_status = 'Cash Out';
            var classs_name = "cash_out_entry";
        }
        var amount = $(".cashbook_amount").val();
        var date = $(".user_date").val();
        var user = $(".show_user_Data option:selected").html();
        var remark = $(".remark_amount").val();
        if (amount != '' && date != '' && user != '') {
            var html = "<div class='user_data_show'><span class='user_name_add'>" + user + "</span><span class='user_amount_add " + classs_name + "'>₹ " + amount + "</span></div>";
            html += "<div class='user_date_add'><span>" + date + "</span></div>";
            html += "<div class='user_remark_add'><span>" + remark + "</span></div>";
            html += "<div class='user_status_add " + classs_name + "'><span>" + cash_status + "</span></div>";
            $(".cashbook_html").html(html);
        } else {
            $(".alert-danger").css({'display': 'block'});
            $(".dataBlank").html("Please Fill All The Field");
        }
        if (cash_status == 'Cash In') {
            cash_status = '0';
        } else {
            cash_status = '1';
        }
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"amount": amount, "date": date, "user": user, "remark": remark, "cash_status": cash_status, "method": "cashbook_insertdata"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
            }
        });
    });
}
function clscashbook_show_data(page, value, call_from) {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"page": page, "value": value, "call_from": call_from, "method": 'cashbook_show_data', "filter_data": filter_cash_array},
        success: function (result) {
            var json_data = JSON.parse(result);
//                var json_data = $.parseJSON(result);
            $(".cashbook_data_html").html(json_data["data"]);
            $(".totalAmount").html("Total = " + json_data["total_amount"]);
﻿            if(json_data["total_amount"] < 0){
                $(".totalAmount").css("color","red");
            }else{
                $(".totalAmount").css("color","green");
            }
        }
    });
}
$(document).on('keyup', '.cls_cashbook_search', function () {
    var value = $(this).val();
    console.log(value);
    filter_cash_array["serach_value"] = value;
    if (this.value.length >= 3 || this.value.length == 0) {
        clscashbook_show_data(1, value, "oncashbook_search");
    }
});
$(document).on("change", '#cashbook_filter_date', function () {
    var date = new Date($('#cashbook_filter_date').val());
    var date_value = $("#cashbook_filter_date").val();
    console.log(date_value);
    filter_cash_array["date_value"] = date_value;
    clscashbook_show_data(1, date_value, "oncashbook_date");
});
$(document).on("change", ".bootstrap-select.show_user_data_filter", function (event) {
    event.preventDefault();
    var status = $('.show_user_data_filter option:selected').html();
    var statu_s = $('.show_user_data_filter option:selected').val();
    console.log(statu_s);
    filter_cash_array["status_html"] = status;
    filter_cash_array["status_value"] = statu_s;
    clscashbook_show_data(1, status, 'oncashbook_status');
});


//baby shower image load page
function set_babyshower_image(data) {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'babyshower_image_set', "data": data},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".baby_flex").html(json_data["data"]);
        }
    });
}


//index.php page slider images set 
function set_slider_image() {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'slider_img_set'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".baby_flex").html(json_data["data"]);
        }
    });
}



//video add page data insert function
$(document).on("click", ".video_link_add", function () {
    var video_link = $(".video_link").val();
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'video_link_insert',"video": video_link},
        success: function (result) {
            var json_data = JSON.parse(result);
            console.log(json_data["msg"]);
            $(".baby_flex").html(json_data["data"]);
            $('.clsvideo').css("display", "block");
            $('.clsvideo').html(json_data["msg"]);
            setTimeout(function () {
                        $('.clsvideo').css("display", "none");
                        set_video_pages();
                    }, 3000);
        }
    });
});


//Set video page table data function
function set_video_pages(){
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'video_get_data'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".video_tbody").html(json_data["data"]);
        }
    });
}


//contact us page send php mailer

$(document).on("click", "#mailerButton", function (event) {
    event.preventDefault();
    console.log("hello");
    return false;
});



//Video gallery page set function
function set_video_gallery_page(){
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'video_gallery_set_data'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".videoGallerySetData").html(json_data["data"]);
        }
    });
}


//Photo gallery page set data
function setPhotogalleryImage(){
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'photo_gallery_set_data'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".photogallerryDiv").html(json_data["data"]);
        }
    });
}



//delete video link on video page
$(document).on("click", ".clsVideoDelete", function () {
    var clsthis = this;
    var clsid = $(this).data("id");
    var result = confirm("Want to delete?");
    if (result == true) {
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"id": clsid, "method": "video_delete_data"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
                if (json_data["result"] == "fail") {
                    $('.clsdel').html(json_data["msg"]);
                } else {
                    $(clsthis).closest("tr").remove();
                    $('.clsdel').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.clsdel').css("display", "none");
                    }, 3000);
                }
            }
        });
    } else {
        alert("something went wrong");
    }
});


// In multistep form when update function data set in the form
function multistep_update(id){
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": id, "method": "multistep_get_data"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            $('.cls_or_num').val(json_data["order_num"]);
            $('#clsselect').val(json_data["order_by"]);
            $('.multiDate').val(json_data["date"]);
            $('.multiTime').val(json_data["time"]);
            $('#categorySelect').val(json_data["type"]);
            $('.clientName').val(json_data["client_name"]);
            $('.clientNumber').val(json_data["client_contact"]);
            $('.advanceAmount').val(json_data["advance_amount"]);
            $('.remainAmount').val(json_data["remain_amount"]);
            $('.totalAmount').val(json_data["total_amount"]);
            $('.address').val(json_data["address"]);
        }});
}


function scroll_fixed_div(){
    if (window.matchMedia('(max-width: 600px)').matches) {
      var fixmeTop = $('.fix_div').offset().top;
        $(window).scroll(function() {
            var currentScroll = $(window).scrollTop();
            if (currentScroll >= fixmeTop) {
                $('.fix_div').css({
                    position: 'fixed',
                    top: '60px',
                    left: '16px',
                    width: '92%',
                    zIndex: '100',
                    backgroundColor: 'white'
                });
            } else {
                $('.fix_div').css({
                    position: 'static'
                });
            }  
        });
    }
}