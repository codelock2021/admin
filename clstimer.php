<?php
include 'function1.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:index.php");
}
$clsid = (isset($_GET['id']) && $_GET['id'] != "") ? $_GET['id'] : '';
$dispplay_data = ($clsid == '') ? "block" : "none";
?>
<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>Employee Dashboard</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/xs/avatar7.jpg" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css"/>
        <link rel="stylesheet" href="assets/plugins/morrisjs/morris.css"/>
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <script src="../nexa/assets/js/ajax.js"></script>
        <script type="text/javascript">
        </script>
    </head>
    <style>
        /*        .timer{
                    text-align: center;
                    font-size: 25px;
                }
                .clock{
                    display: flex;
                    justify-content: center;
                }*/
        .container{
            text-align: center;
            color: #fff;
            height: 115px;
            padding: 30px;
            border-radius: 0px;
            font-family: sans-serif;
            width: 60% !important;
        }
        .display{
            color: #FFF;
            padding: 5px;
            border-radius: 20px;
        }
        #display{
            font-size: 30px;
            padding-bottom: 20px;
            color: rgba(255,255,255,.3);
        }
        .btn{
            padding: 0px !important;
            height: 40px;
            width: 125px;
            margin: 20px;
            border-radius: 4px !important;
            border: none;
            font-size: 23px !important;
            color: white;
            font-family: 'Poppins', sans-serif;
        }
        .clsdiv{
            background: linear-gradient(45deg, #1870ed 0, #f18f88 100%);
            border-radius: 10px !important;
            width: 50%;
            margin-left: 25%;   
        }
        #start{
            background: #73d773;
        }
        #stop{
            background: #F00000;
        }
        #reset{
            background: #48bfe3;
        }
        #lap{
            background: #5a189a;
        }
        .center{
            display: flex;
            justify-content: center;
            align-items: center;
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }				
        .hide{
            display: none;
        }
        .clscon{
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }
        .members_profiles{
            margin-top: 40px;
            text-align: center;
        }
        .clstr{
            margin-top: 50px;
        }
        .clsth{
            width: 50%;
        }
        .navbar-brand{
            margin-left: 30px;
            margin-top: 13px;
        }
        .navbar-header{
            padding-bottom: 8px;
            padding-top: 8px;
        }
        .clsempt{
            margin-top: 10px;
        }
        .clstmsearch{
            text-align: center;
            width: 55%;
            margin-top: 40px;
            height: 35px;
        }
        .clstmdiv{
            justify-content: center;
            display: flex;
        }
        .bars img{
            border-radius: 100px;
        }
        .material-icons{
            margin-top: 45px;
            position: absolute;
            margin-left: 52%;
        }
    </style>
    <body class="theme-orange">
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="col-12">
                <div class="navbar-header">

                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="logout.php" class="mega-menu xs-hide" data-close="true"><i class="zmdi zmdi-power"></i></a></li>
                </ul>
            </div>
        </nav>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
        </aside>
        <!-- Main Content -->
        <section class="content home">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12 clsempt">
                        <h2>Employee Dashboard</h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item"><a href="Dashboard.php"><i class="zmdi zmdi-home"></i> Codelock</a></li>
                            <li class="breadcrumb-item active">Dashboard 1</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--                        <div class="clock">
                                        <div class="timer">
                                            <span class="hours">00</span>:<span class="minutes">00</span>:<span class="seconds">00</span>
                                        </div>
                                    </div>-->

            <div class="clsdiv" style="display: <?php echo $dispplay_data; ?>">
                <div class="container clscon">
                    <div class="display">
                        <h1 id="time"><span class="hours">00</span>:<span class="minutes">00</span>:<span class="seconds">00</span></h1>
                    </div>
                    <p id="display">00:00:00</p>
                </div>
                <div class="container center">
                    <button id="start" class="btn"onclick="start()">Start</button>
                    <button class="hide btn" id="stop" onclick="stop()">Stop</button>
                    <button class="hide btn" id="lap" onclick="lap()">Lap</button>
                    <button id="reset" class="btn" onclick="reset()">Reset</button>
                </div>
            </div>
            <div class="clstmdiv">
                <i class="material-icons">search</i>
                <input type="search" class="clstmsearch" data-table="order-table" placeholder="search at least 10 characters formet(YYYY-MM-DD)"/>   
            </div>
            <div class="body table-responsive members_profiles">
                <table class="table table-hover">
                    <thead>
                        <tr class="clstr">
                            <th class="clsth">Create at Date</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody class="clstbody">
                    </tbody>
                </table>
            </div>
        </section>
        <!-- Jquery Core Js -->
        <script src="assets/bundles/libscripts.bundle.js"></script>
        <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/vendorscripts.bundle.js"></script>
        <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/jvectormap.bundle.js"></script>
        <!-- JVectorMap Plugin Js -->
        <script src="assets/bundles/morrisscripts.bundle.js"></script>
        <!-- Morris Plugin Js -->
        <script src="assets/bundles/sparkline.bundle.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </body>
    <script>
        getExistingTime();
    </script>
</html>
