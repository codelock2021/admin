<?php

include('smtp/PHPMailerAutoload.php');

class Register {

//    public $host = "localhost";
//    public $user = "u402017191_image_gallery";
//    public $password = "Image_gallery@99";
//    public $db = "u402017191_image_gallery";
    public $host = "localhost";
    public $user = "root";
    public $password = "";
    public $db = "image_gallery";
    var $connect_db;

    function __construct() {

        $this->connect_db = new mysqli($this->host, $this->user, $this->password, $this->db);

        if (mysqli_connect_errno()) {
            printf("Connection failed:", mysqli_connect_error());
            exit();
        }
        return true;
    }

    function clssignin() {
        session_start();
        $useremail = $_POST['email'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM image_login WHERE email='$useremail' AND  password='$password'";
        $result = $this->connect_db->query($sql);
        $ud = $result->fetch_assoc();
        $total_records = mysqli_num_rows($result);
        if ($total_records > 0) {
            $_SESSION['id'] = $ud['id'];
            $_SESSION['email'] = $ud['email'];
            $_SESSION['password'] = $ud['password'];
            $response["msg"] = "success";
            $response["data"] = "Login Successfully";
        } else {
            $response["msg"] = "fail";
            $response["data"] = "Invalid Username & Password";
        }
        echo json_encode($response);
    }

    
    //image gallery image set data
    function set_image() {
        $response = array();
        $ctgryid = $_POST['value'];
        $output = '';
        $record_per_page = 24;
        $page = $i = 1;
        if (isset($_POST["page"])) {
//            $start = (($_POST['page'] - 1) * $limit);
            $page = $_POST["page"];
        }
        $start_from = ($page - 1) * $record_per_page;
        if ($ctgryid == '100') {
            $query = "SELECT * FROM cls_image ORDER BY status =1, id DESC LIMIT $start_from, $record_per_page";
        } else {
            $query = "SELECT * FROM cls_image where category='$ctgryid' AND status = '1' LIMIT $start_from, $record_per_page";
        }
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $category = $row['category'];
            $id = $row['id'];
            $image = $row['file'];
            $output .= "<div class='clsdels' id='clsdels" . $id . "'>
                <img class='clsprev' src='assets/images/" . $image . "' height='250' width='250' />
                <button class='btn  btn-raised btn-danger waves-effect clsremove' data-id='" . $id . "'>X</button>
                </div>";
        };
        $output_page = "<div class='page_center'>";
        if ($ctgryid == '100') {
            $page_query = "SELECT * FROM cls_image ORDER BY id DESC";
        } else {
            $page_query = "SELECT * FROM cls_image where category='$ctgryid'";
        }
        $page_result = mysqli_query($this->connect_db, $page_query);
        $total_records = mysqli_num_rows($page_result);
        if ($total_records == 0) {
            $response['result'] = "Fail";
            $response['error'] = "Data Not Found";
        } else if ($total_records > $record_per_page) {
            $total_pages = ceil($total_records / $record_per_page);
            for ($i = 1; $i <= $total_pages; $i++) {
                $output_page .= '<li class="page-item clsCls' . $i . '" aria-current="page">
                    <span class="page-link pgntn_link" id="' . $i . '">' . $i . '</span>
                    </li>';
            }
            $output_page .= "</div>";
            $response["result"] = "success";
            $response["msg"] = "success";
        }
        $response["data"] = $output;
        $response["pagination"] = $output_page;
        echo json_encode($response);
    }

    
    //image gallery images de
    function clsdelete_data() {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "update cls_image set status ='0' where id=$clsid";
//        $clsdelete = "DELETE  FROM cls_image WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) == TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail! ";
        }
        echo json_encode($deletemsg);
    }

    
    //image gallery page image upload function
    function clsimage() {
        $response = $error_array = array();
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
            $file_name = $_FILES['image']['name'];
        } else {
            $error_array["image"] = "Image is Require!";
        }
        if (isset($_POST['category']) && $_POST['category'] != "") {
            $category = $_POST['category'];
        } else {
            $error_array["category"] = "Category is Require!";
        }
        $count = count($file_name);
        if ($count < 16) {
            
        } else {
            $error_array["size"] = "You Only add 15 number of Image at Time !";
        }
        if (!empty($error_array)) {
            $response["result"] = "fail";
            $response["error"] = $error_array;
        } else {
            foreach ($_FILES['image']["name"] as $key => $value) {
                $src = $_FILES['image']['tmp_name'][$key];
                $dest = $_FILES['image']['name'][$key];
                $imageFileType = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
                if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png" || $imageFileType == "gif" || $imageFileType == "bmp") {
                    $upload = move_uploaded_file($src, "assets/images/" . $dest);
                    $sql = "INSERT INTO cls_image (id,file,category) VALUES (' ','$dest',$category)";
                    if ($this->connect_db->query($sql) === TRUE) {
                        $response["result"] = "success";
                        $response["msg"] = "Uploaded Images Successfully! ";
                    } else {
                        $response["result"] = "fail";
                        $response["msg"] = "Upload Image Fail! ";
                    }
                } else {
                    $response["msg"] = "Please Insert jpg, jpeg, gif, bmp and png Files!";
                }
            }
        }
        echo json_encode($response);
    }

    //for Multistep form insert data
    function insert_data() {
        $account_id = (isset($_POST['account_id']) && $_POST['account_id'] != "") ? $_POST['account_id'] : '';
        $response = array();
        if ($account_id == '') {
            $error_array = array() == "";
            if (isset($_POST['order_by']) && $_POST['order_by'] != "") {
                $order_by = $_POST['order_by'];
            } else {
                $error_array = "Order by is Require! ,";
            }
            if (isset($_POST['date']) && $_POST['date'] != "") {
                $date = $_POST['date'];
            } else {
                $error_array .= "Date is Require! ,";
            }
            if (isset($_POST['time']) && $_POST['time'] != "") {
                $time = $_POST['time'];
            } else {
                $error_array .= "Time is Require! ,";
            }
            if (isset($_POST['cls_type']) && $_POST['cls_type'] != "") {
                $type = $_POST['cls_type'];
            } else {
                $error_array .= "Type is Require! ,";
            }
            if (isset($_POST['client_name']) && $_POST['client_name'] != "") {
                $client_name = $_POST['client_name'];
            } else {
                $error_array .= "Client Name is Require! ,";
            }
            if (isset($_POST['client_num']) && $_POST['client_num'] != "") {
                $client_num = $_POST['client_num'];
            } else {
                $error_array .= "Client Number is Require! ,";
            }
            if (isset($_POST['advance_amount']) && $_POST['advance_amount'] != "") {
                $advance_amount = $_POST['advance_amount'];
            } else {
                $error_array .= "Advance Amount is Require! ,";
            }
            if (isset($_POST['remain']) && $_POST['remain'] != "") {
                $remain = $_POST['remain'];
            } else {
                $error_array .= "Remain Amount is Require! ,";
            }
            if (isset($_POST['total_amount']) && $_POST['total_amount'] != "") {
                $total_amount = $_POST['total_amount'];
            } else {
                $error_array .= "Total Amount is Require!";
            }
            $destination_array = explode(',', $error_array);
            if (!empty($error_array)) {
                $response["result"] = "fail";
                $response["data"] = $destination_array;
            } else {
                if ($_POST["id"] == '') {
                    $order_num = $_POST['order_num'];
                    $address = $_POST['address'];
                    $sql = "INSERT INTO multistep_form (id, order_num, order_by, date, time, type, client_name, client_contact, advance_amount, remain_amount, total_amount, address) VALUES "
                            . "('', '$order_num', '$order_by', '$date', '$time', '$type', '$client_name', $client_num, $advance_amount, $remain, $total_amount, '$address')";
                    if ($this->connect_db->query($sql) === TRUE) {
                        $response["result"] = "success";
                        $response["msg"] = "Insert Successfully! ";
                        $select = "select * from multistep_form where order_num ='$order_num'";
                        $result = $this->connect_db->query($select);
                        $row = mysqli_fetch_array($result);
                        $formid = $row['id'];
                        $_SESSION['id'] = $formid;
                        $response["account_id"] = $formid;
                    } else {
                        $response["result"] = "fail";
                        $response["msg"] = "Insert Fail! ";
                    }
                }
            }
        } else {
            $foil_ball = array();
            $ball_color = $_POST['ball_color'];
//            $foil_ball = array(
//                "foil_input" => $_POST['foil_ball'],
//                "foil_qty" => $_POST['foil_ball_qty'],
//                "foil_color" => $_POST['foil_ball_color']
//            );
            $foil_ball = $_POST['foil_ball'] . ', ' . $_POST['foil_ball_qty'] . ', ' . $_POST['foil_ball_color'];
            $gate = $_POST['gate'];
            $backdrop = $_POST['backdrop'];
            $banner = $_POST['banner_select'];
            $pillar = $_POST['pillar_lable'] . ', ' . $_POST['pillar_qty'];
            $props = $_POST['props'] . ', ' . $_POST['props_select'];
            $stage = $_POST['stage'] . ', ' . $_POST['stage_input'];
            $sofa = $_POST['sofa'];
            $fog_machine = $_POST['fog'];
            $matka_qty = $_POST['matka_qty'];
            $payro_qty = $_POST['payro_qty'];
            $light_qty = $_POST['light_qty'];
            $other_info = $_POST['other_info'];
            $customer_img_name = $_FILES['customer_img']['name'];
            $customer_img_tmp_name = $_FILES['customer_img']['tmp_name'];
            $upload = move_uploaded_file($customer_img_tmp_name, "assets/images/" . $customer_img_name);
            $clsup = "update multistep_form set balloon_colors='$ball_color' ,foil_balloon='$foil_ball' ,gate='$gate', backdrop='$backdrop', Banner='$banner', pillar='$pillar', props='$props', stage='$stage', sofo='$sofa', fog_matka='$matka_qty', fog_machine='$fog_machine', cold_payro='$payro_qty', par_light='$payro_qty', other_info='$other_info', customer_image='$customer_img_name' where id='$account_id'";
            if ($this->connect_db->query($clsup) === TRUE) {
                $select = "select * from multistep_form where id ='$account_id'";
                $result = $this->connect_db->query($select);
                $row = mysqli_fetch_array($result);
                $formid = $row['id'];
                $_SESSION['id'] = $formid;
                $response["account_id"] = $formid;
                $response["result"] = "success";
                $response["msg"] = "Update Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Update Fail! ";
            }
        }
        echo json_encode($response);
    }

    //multiple order
    function set_data() {
        $response = array();
        $filter_check = false;
        $i = $page = 1;
        $output = '';
        $limit = 10;
        $start = 0;
        if (isset($_POST["page"]) && $_POST["page"] != "") {
            $start = (($_POST['page'] - 1) * $limit);
            $page = $_POST["page"];
        }
        if (isset($_POST['call_from']) && $_POST['call_from'] == "on_load") {
            $query = "SELECT * FROM multistep_form ORDER BY status = 1, id DESC LIMIT $start, $limit";
        } else if (isset($_POST['call_from']) && ($_POST['call_from'] == "on_search" || $_POST['call_from'] == "on_date_filter")) {
            $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%" OR date LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
            $response["pagination"] = "";
        } else if (isset($_POST['call_from']) && $_POST['call_from'] == "on_status_change") {
            $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
            $response["pagination"] = "";
        }
        if (isset($_POST["filter_data"]) && !empty($_POST["filter_data"])) {
            foreach ($_POST["filter_data"] as $key => $value) {
                if ($value != "") {
                    $filter_check = true;
                }
            }
            if (isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] == "" && isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] == "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] == "" && isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] == "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "" && isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] == "" && isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] == "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] == "" && isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] == "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["status_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "" && isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%" AND order_status LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["status_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "" && isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["status_value"]) . '%" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "" && isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%" AND order_status LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["status_value"]) . '%"';
                $response["pagination"] = "";
            }
        }
        if (!isset($_POST['filter_data']) || $filter_check == false) {
            $query = "SELECT * FROM multistep_form ORDER BY status = 1, id DESC LIMIT $start, $limit";
            $page_query = "SELECT * FROM multistep_form ORDER BY id DESC";
            $page_result = mysqli_query($this->connect_db, $page_query);
            $total_data = mysqli_num_rows($page_result);
            $total_links = ceil($total_data / $limit);
            $previous_link = $next_link = $page_link = $output_page = '';
            $page_array = array();
            if ($total_links > 4) {
                if ($page < 5) {
                    for ($count = 1; $count <= 5; $count++) {
                        $page_array[] = $count;
                    }
                    $page_array[] = '...';
                    $page_array[] = $total_links;
                } else {
                    $end_limit = $total_links - 5;
                    if ($page > $end_limit) {
                        $page_array[] = 1;
                        $page_array[] = '...';
                        for ($count = $end_limit; $count <= $total_links; $count++) {
                            $page_array[] = $count;
                        }
                    } else {
                        $page_array[] = 1;
                        $page_array[] = '...';
                        for ($count = $page - 1; $count <= $page + 1; $count++) {
                            $page_array[] = $count;
                        }
                        $page_array[] = '...';
                        $page_array[] = $total_links;
                    }
                }
            } else {
                for ($count = 1; $count <= $total_links; $count++) {
                    $page_array[] = $count;
                }
            }
            for ($count = 0; $count < count($page_array); $count++) {
                if ($page == $page_array[$count]) {
                    $previous_id = $page_array[$count] - 1;
                    $page_link .= '<li class="page-item  active clsClass"><a class="page-link " href="javascript:void(0)">' . $page_array[$count] . ' <span class="sr-only pagination_link" id="' . $page_array[$count] . '">(current)</span></a>
                                    </li>';
                    if ($previous_id > 0) {
                        $previous_link = '<li class="page-item clsClass"><a class="page-link pagination_link" id="' . $previous_id . '"  href="javascript:void(0)" data-page_number="' . $previous_id . '">Previous</a></li>';
                    } else {
                        $previous_link = '<li class="page-item disabled"><a class="page-link pagination_link" id="' . $previous_id . '" href="javascript:void(0)">Previous</a>
                                            </li>';
                    }
                    $next_id = $page_array[$count] + 1;
                    if ($next_id > $total_links) {
                        $next_link = '<li class="page-item disabled clsClass"><a class="page-link pagination_link" id="' . $next_id . '" href="#">Next</a></li>';
                    } else {
                        $next_link = '<li class="page-item clsClass"><a class="page-link pagination_link" id="' . $next_id . '" href="javascript:void(0)" data-page_number="' . $next_id . '">Next</a></li>';
                    }
                } else {
                    if ($page_array[$count] == '...') {
                        $page_link .= '<li class="page-item disabled clsClass"><a class="page-link pagination_link" id="' . $page_array[$count] . '" href="#">...</a>
                                                    </li>';
                    } else {
                        $page_link .= '<li class="page-item clsClass"><a class="page-link pagination_link" id="' . $page_array[$count] . '" href="javascript:void(0)" data-page_number="' . $page_array[$count] . '">' . $page_array[$count] . '</a></li>';
                    }
                }
            }
            if ($total_data > 10) {
                $output_page .= $previous_link . $page_link . $next_link;
            }
            $response["pagination"] = $output_page;
        } else {
            $response["pagination"] = "";
        }
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $id = $row['id'];
            $slNo = $i + $start;
            $selected_status = $row['order_status'];
            $status_selected = "selected='selected'";
            $selectd_bg_color = ($selected_status == "0") ? 'panding_color' : (($selected_status == "1") ? 'complete_color' : (($selected_status == "2") ? 'cancel_color' : 'done_color'));
            $selection_html = "<select class='show-tick show_order_status $selectd_bg_color'  name='clsorder_status'>
                                <option value='0' data-id=" . $id . " name='clsorder_status' class='panding_color' " . ($selected_status == '0' ? $status_selected : '') . ">Panding</option>
                                <option value='1' data-id=" . $id . " name='clsorder_status' class='complete_color' " . ($selected_status == '1' ? $status_selected : '') . ">Complete</option>
                                <option value='2' data-id=" . $id . " name='clsorder_status' class='cancel_color' " . ($selected_status == '2' ? $status_selected : '') . ">Cancel</option>
                                <option value='3' data-id=" . $id . " name='clsorder_status' class='done_color' " . ($selected_status == '3' ? $status_selected : '') . ">Done</option>
                            </select>";
            $output .= "<tr id=tr_" . $id . ">   
                <td><input type='checkbox' class='muldel' name='checkbox[]' id=" . $id . "></td>
                <td>" . $slNo . "</td>
                <td>" . $row["order_num"] . "</td>
                <td class='clsme'>" . $row["date"] . "</td>
                <td class='clsme'>" . $row["time"] . "</td>
                <td class='clsme'>" . $row["client_contact"] . "</td>
                <td class='clsme'>" . $row["advance_amount"] . "</td>
                <td class='clsme'>" . $row["remain_amount"] . "</td>
                <td class='clsme'>" . $row["total_amount"] . "</td>
                <td class='btn_td'><a class='btn btn-danger clsdelete btn-outline-dark' data-id='" . $id . "'><i class='fa fa-trash-o' aria-hidden='true'></i></a>   
                    <a class='btn btn1 btn-outline-dark' href='multistep_form.php?id=" . $id . "'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                    <a class='btn  btn-raised bg-black waves-effect waves-light clsview' href='invoice_data.php?id=" . $id . "'><i class='fa fa-eye' aria-hidden='true'></i></a>
                    " . $selection_html . "   
                </td>
            </tr>";
            $i++;
        };
        $response["result"] = "success";
        $response["msg"] = "success";
        $response["data"] = $output;
        echo json_encode($response);
    }

    function set_order_status() {
        $order_value = $_POST['dropdown'];
        $order_id = $_POST['id'];
        $order_status = $_POST['class'];
        $query = "update multistep_form set order_status ='$order_value' where id='$order_id'";
        if ($this->connect_db->query($query) === TRUE) {
            $response["result"] = "success";
            $response["class"] = $order_status;
            $response["msg"] = "Update Successfully! ";
        } else {
            $response["result"] = "fail";
            $response["msg"] = "Update Fail! ";
        }
        echo json_encode($response);
    }

    function multidelete() {
        $delete = $_POST['id'];
        $deletemsg = array();
        foreach ($delete as $id) {
            $query = "update multistep_form set status ='0' where id = '" . $id . "'";
//            $query = "DELETE FROM multistep_form WHERE id = '" . $id . "'";
            $result = mysqli_query($this->connect_db, $query) or die("Invalid query");
        }
        if (mysqli_affected_rows($this->connect_db) > 0) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Delete Successfully";
            $deletemsg["id"] = $delete;
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail";
        }
        echo json_encode($deletemsg);
    }

    function delete_data() {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "update multistep_form set status ='0' where id =" . $clsid;
//        $clsdelete = "DELETE  FROM multistep_form WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail! ";
        }
        echo json_encode($deletemsg);
    }

    function get_data() {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : '';
        $clsselect = "select * from multistep_form where id=" . $clsid;
        $cls = $this->connect_db->query($clsselect);
        $row = $cls->fetch_assoc();
        echo json_encode($row);
    }

    //set a order numer on multistep form
    function set_order_num() {
        $response = '';
        $max_number = "SELECT max(order_num) from multistep_form";
        $get_max_num = $this->connect_db->query($max_number);
        $row = mysqli_fetch_assoc($get_max_num);
        $max_order_id = $row['max(order_num)'];
        $max_order_id++;
        $response = $max_order_id;
        echo json_encode($response);
    }

    //making a invoice data function
    function invoice_data() {
        $clsoutput = $customer_html = $output = $item_html = '';
        $response = array("result" => "fail", "msg" => "Something went wrong");
        $id = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : '';
        $sql = "select * from multistep_form where id=" . $id;
        $cls = $this->connect_db->query($sql);
        $ingore_array = array("id", "order_num", "order_by", "date", "time", "type", "client_name", "client_contact", "address", "advance_amount", "total_amount", "remain_amount", "created_at", "updated_at");
        $amount_array = array("advance_amount", "remain_amount", "total_amount");
        while ($clsrow = mysqli_fetch_object($cls)) {
            foreach ($clsrow as $key => $value) {
                if (in_array($key, $ingore_array)) {
                    continue;
                } else {
                    if ($key == 'balloon_colors') {
                        $key = ucfirst($key);
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $clsrow->balloon_colors . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'foil_balloon') {
                        $key = ucfirst($key);
                        $comma = ',';
                        $foil_ball = $clsrow->foil_balloon;
                        if ($foil_ball == "") {
                            $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td></td>
                            <td></td>
                        </tr>";
                        } else if (strpos($foil_ball, $comma) !== false) {
                            $explod_foil_ball = explode(",", $foil_ball);
                            $foil_deatails = $explod_foil_ball[0] . ',' . $explod_foil_ball[2];
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $foil_deatails . "</td>
                                <td class='clsme'>" . $explod_foil_ball[1] . "</td>
                            </tr>";
                        } else {
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $foil_ball . "</td>
                                <td></td>
                            </tr>";
                        }
                    }
                    if ($key == 'gate') {
                        $key = ucfirst($key);
                        $cls_gate = $clsrow->gate;
                        if ($cls_gate == '1') {
                            $cls_gate = 'Yes';
                        } else {
                            $cls_gate = 'No';
                        }
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_gate . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'backdrop') {
                        $key = ucfirst($key);
                        $cls_backdrop = $clsrow->backdrop;
                        if ($cls_backdrop == '1') {
                            $cls_backdrop = 'Yes';
                        } else {
                            $cls_backdrop = 'No';
                        }
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_backdrop . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'Banner') {
                        $key = ucfirst($key);
                        $cls_Banner = $clsrow->Banner;
                        if ($cls_Banner == '1') {
                            $cls_Banner = 'Text1';
                        } else if ($cls_Banner == '2') {
                            $cls_Banner = 'Text2';
                        } else {
                            $cls_Banner = 'Text3';
                        }
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_Banner . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'pillar') {
                        $key = ucfirst($key);
                        $comma = ',';
                        $pillar = $clsrow->pillar;
                        if ($pillar == "") {
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td></td>
                                <td></td>
                            </tr>";
                        } else if (strpos($pillar, $comma) !== false) {
                            $explod_pillar = explode(",", $pillar);
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $explod_pillar[0] . "</td>
                                <td class='clsme'>" . $explod_pillar[1] . "</td>
                            </tr>";
                        } else {
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $pillar . "</td>
                                <td></td>
                            </tr>";
                        }
                    }
                    if ($key == 'props') {
                        $key = ucfirst($key);
                        $cls_props = $clsrow->props;
                        $explod_props = explode(",", $cls_props);
                        if ($explod_props[0] == '1') {
                            $explod_props[0] = "Yes";
                            if ($explod_props[1] == '1') {
                                $explod_props[1] = 'Text1';
                            } else if ($explod_props[1] == '2') {
                                $explod_props[1] = 'Text2';
                            } else {
                                $explod_props[1] = 'Text3';
                            }
                            $cls_props = $explod_props[0] . ',' . $explod_props[1];
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $cls_props . "</td>
                                <td></td>
                            </tr>";
                        } else {
                            $explod_props[0] = "No";
                            $item_html .= "<tr>
                                <td>" . ucfirst($key) . "</td>
                                <td class='clsme'>" . $explod_props[0] . "</td>
                                <td></td>
                            </tr>";
                        }
                    }
                    if ($key == 'stage') {
                        $cls_stage = $clsrow->stage;
                        $explod_stage = explode(",", $cls_stage);
                        if ($explod_stage[0] == '1') {
                            $explod_stage[0] = "Yes";
                            $cls_stage = $explod_stage[0] . ',' . $explod_stage[1];
                            $item_html .= "<tr>
                                <td>" . ucfirst($key) . "</td>
                                <td class='clsme'>" . $cls_stage . "</td>
                                <td></td>
                            </tr>";
                        } else {
                            $explod_stage[0] = "No";
                            $item_html .= "<tr>
                                <td>" . ucfirst($key) . "</td>
                                <td class='clsme'>" . $explod_props[0] . "</td>
                                <td></td>
                            </tr>";
                        }
                    }
                    if ($key == 'sofo') {
                        $cls_sofo = $clsrow->sofo;
                        if ($cls_sofo == '1') {
                            $cls_sofo = 'Yes';
                        } else {
                            $cls_sofo = 'No';
                        }
                        $item_html .= "<tr>
                            <td>" . ucfirst($key) . "</td>
                            <td class='clsme'>" . $cls_sofo . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'fog_machine') {
                        $key = ucfirst($key);
                        $cls_fog_machine = $clsrow->fog_machine;
                        if ($cls_fog_machine == '1') {
                            $cls_fog_machine = 'Yes';
                        } else {
                            $cls_fog_machine = 'No';
                        }
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_fog_machine . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'fog_matka') {
                        $key = ucfirst($key);
                        $cls_fog_matka = $clsrow->fog_matka;
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td></td>
                            <td class='clsme'>" . $cls_fog_matka . "</td>
                        </tr>";
                    }
                    if ($key == 'cold_payro') {
                        $key = ucfirst($key);
                        $cls_cold_payro = $clsrow->cold_payro;
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td></td>
                            <td class='clsme'>" . $cls_cold_payro . "</td>
                        </tr>";
                    }
                    if ($key == 'par_light') {
                        $key = ucfirst($key);
                        $cls_par_light = $clsrow->par_light;
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td></td>
                            <td class='clsme'>" . $cls_par_light . "</td>
                        </tr>";
                    }
                    if ($key == 'other_info') {
                        $key = ucfirst($key);
                        $cls_other_info = $clsrow->other_info;
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_other_info . "</td>
                            <td></td>
                        </tr>";
                    }
                }
            }
            foreach ($clsrow as $key => $value) {
                if ($key == 'advance_amount') {
                    $key = ucfirst($key);
                    $item_html .= "<tr>
                            <td></td>
                            <td class='clsme'>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $clsrow->advance_amount . "</td>
                        </tr>";
                }
                if ($key == 'remain_amount') {
                    $key = ucfirst($key);
                    $item_html .= "<tr>
                            <td></td>
                            <td class='clsme'>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $clsrow->remain_amount . "</td>
                        </tr>";
                }
                if ($key == 'total_amount') {
                    $key = ucfirst($key);
                    $item_html .= "<tr>
                            <td></td>
                            <td class='clsme'>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $clsrow->total_amount . "</td>
                        </tr>";
                }
            }
            $customer_html .= "<tr>
                <td>" . $clsrow->order_num . "</td>
                <td>" . ucfirst($clsrow->client_name) . "</td>
                <td>" . $clsrow->client_contact . "</td>
                <td class='customer_address'>" . $clsrow->address . "</td>
                <td class='td_date'>" . $clsrow->date . "</td>
                <td>" . $clsrow->time . "</td>
            </tr>";
            $customer_mobile_html = "<tr><th>Order Number</th><td>" . $clsrow->order_num . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Customer Name</th><td>" . ucfirst($clsrow->client_name) . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Customer Number</th><td>" . $clsrow->client_contact . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Address</th><td>" . $clsrow->address . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Date</th><td>" . $clsrow->date . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Time</th><td>" . $clsrow->time . "</td></tr>";
        }
        if ($customer_html != "") {
            $response["result"] = "success";
            $response["msg"] = "success";
            $response["customer_data"] = $customer_html;
            $response["customer_mobile_data"] = $customer_mobile_html;
            $response["item_data"] = $item_html;
        }
        echo json_encode($response);
    }

    //cashbook page functions
    //cashbook data insert
    function cashbook_insertdata() {
        $date = $_POST['date'];
        $user_name = $_POST['user'];
        $cashbook_amount = $_POST['amount'];
        $remark_amount = $_POST['remark'];
        $button_value = $_POST['cash_status'];
        $sql = "INSERT INTO cashbook_data (id,user_id,user_name,date,amount,remark,cash_status) VALUES (' ',' ','$user_name','$date',$cashbook_amount,'$remark_amount','$button_value')";
        if ($this->connect_db->query($sql) === TRUE) {
            $response["result"] = "success";
            $response["msg"] = "Insert cash book Successfully! ";
        } else {
            $response["result"] = "fail";
            $response["msg"] = "Inser cash book Fail! ";
        }
        echo json_encode($response);
    }

    function cashbook_show_data() {
        $i = 0;
        $data_html = '';
        $response = array();
        if(isset($_POST['filter_data']) && $_POST['filter_data']['status_html'] != ""){
            $part_category = $_POST['filter_data']['status_html'];
            $in_amount = "select sum(amount) from cashbook_data where cash_status = '0' and user_name='$part_category'";
            $out_amount = "select sum(amount) from cashbook_data where cash_status = '1' and user_name='$part_category'";
        }else{
            $in_amount = "select sum(amount) from cashbook_data where cash_status = '0'";
            $out_amount = "select sum(amount) from cashbook_data where cash_status = '1'";
        }
        $in_result = mysqli_query($this->connect_db, $in_amount);
        $in_row = mysqli_fetch_array($in_result);
        $out_result = mysqli_query($this->connect_db, $out_amount);
        $out_row = mysqli_fetch_array($out_result);
        $total_amount = $in_row['0'] - $out_row['0'];
        $query = "SELECT * FROM cashbook_data ORDER BY id DESC";
        if (isset($_POST['call_from']) && ($_POST['call_from'] == "oncashbook_search" || $_POST['call_from'] == "oncashbook_date")) {
            $query = 'SELECT * FROM cashbook_data WHERE remark LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%" OR date LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
        } else if (isset($_POST['call_from']) && $_POST['call_from'] == "oncashbook_status") {
            if ($_POST['value'] == 'User') {
                $query = "SELECT * FROM cashbook_data ORDER BY id DESC";
            } else {
                $query = 'SELECT * FROM cashbook_data WHERE user_name LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
            }
        }
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $user_name = $row['user_name'];
            $amount = $row['amount'];
            $date = $row['date'];
            $remark = $row['remark'];
            $cash_status = $row['cash_status'];
            if ($cash_status == '0') {
                $cash_status = "Cash In";
                $cash_color = "cash_in_color";
            } else {
                $cash_status = "Cash out";
                $cash_color = "cash_out_color";
            }
            $i++;
            $data_html .= "<div class='border_classs'><div class='user_data_show'>" . $i . ".<span class='user_name_add'>" . $user_name . "</span><span class='user_amount_add $cash_color'>₹ " . $amount . "</span></div>
                <div class='user_date_add'><span>" . $date . "</span></div>
                <div class='user_remark_add'><span>" . $remark . "</span></div>
                <div class='user_status_add $cash_color'><span>" . $cash_status . "</span></div></div>";
            $response["result"] = "success";
            $response["data"] = $data_html;
        }
        $response["total_amount"] = $total_amount;
        echo json_encode($response);
    }

    //baby shower page data load and show image
    function babyshower_image_set() {
        $category = $_POST['data'];
        $category_name = "SELECT category FROM category_data where id='$category'";
        $result = mysqli_query($this->connect_db, $category_name);
        $rowctgry = mysqli_fetch_array($result);
        $title = $rowctgry['category'];
        $output = '';
        $query = "SELECT * FROM cls_image where category='$category' ORDER BY ID DESC";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $id = $row['id'];
            $image = $row['file'];
            $output .= "<div class='baby " . $id . " babyimage'><img src='assets/images/" . $image . "' alt='baby1' class='event_img' title='" . $title . "'></div>";
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }

    
    
    //video gallery page data insert function
    function video_link_insert() {
        if (isset($_POST['video']) && $_POST['video'] != "") {
            $video_link = $_POST['video'];
            $query = "INSERT INTO video_gallery (id,video_link) VALUES (' ','$video_link')";
            if ($this->connect_db->query($query) === TRUE) {
                $response["result"] = "success";
                $response["msg"] = "Uploaded Video Link Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Uploaded Video Link Fail! ";
            }
        } else {
            $response["msg"] = "Video Link is Require!";
        }
        echo json_encode($response);
    }

    //video gallery page data show function
    function video_get_data() {
        $output = '';
        $i = 1;
        $query = 'SELECT * FROM video_gallery WHERE status = "1" AND id';
//        $query = "SELECT * FROM video_gallery ORDER BY status = 1, id DESC";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $id = $row['id'];
            $video_link = $row['video_link'];
            $output .= "<tr id=tr_" . $id . ">
                <td>" . $i . "</td>
                <td>" . $video_link . "</td>
                <td class='btn_td'><a class='btn btn-danger clsVideoDelete btn-outline-dark' data-id='" . $id . "'><i class='fa fa-trash-o' aria-hidden='true'></i></a>   
                </td>
            </tr>";
            $i++;
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }
    
    
    
    //Video gallery page set function
    function video_gallery_set_data(){
        $output = '';
        $query = "SELECT * FROM video_gallery where status='1' AND id";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $video_link = $row['video_link'];
            $output .= "<div class='video50'>
                            <div class='evenr'>
                                <iframe width='560' height='400' src='" . $video_link . "' title='YouTube video player' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                            </div>
                        </div>";
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }
    
    
    
    //photo gallery page set data function
    function photo_gallery_set_data(){
        $output = '';
        $query = "SELECT * FROM cls_image ORDER BY ID DESC";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $image = $row['file'];
            $output .= "<div class='baby25'>
                        <img src='assets/images/" . $image . "' alt='gallary1' class='event_img' title='galary'>
                    </div>";
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }
    
    
    
    //video gallery page delete function
    function video_delete_data(){
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "update video_gallery set status ='0' where id =" . $clsid;
//        $clsdelete = "DELETE  FROM multistep_form WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Video Link Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Video Link Fail! ";
        }
        echo json_encode($deletemsg);
    }
    
    
    //multistep form set data
    function multistep_get_data(){
        $id = $_POST['id'];
        $query = "SELECT * FROM multistep_form where id = '$id'";
        $cls = $this->connect_db->query($query);
        $row = $cls->fetch_assoc();
        echo json_encode($row);
    }
    
    function apicall(){
//        $title = $_POST['title'];
//        $image = $_POST['image'];
//        $descri = $_POST['descri'];
        $api_fields = array('product' => array('title' => $_POST["title"], 'body_html' => $_POST["descri"]));
        $api_articles = array("api_name" => "products");
        $set_position = $this->cls_get_shopify_list($api_articles, $api_fields, 'POST', 1, array("Content-Type: application/json"));
        if(!empty($set_position->product->id)){
            $response['result'] = "success";
            $response['data'] = "Product Added Successfully";
        }else{
            $response['result'] = "fail";
            $response['data'] = "Product Added Fail";
        }
        echo json_encode($response);
    }

    function cls_get_shopify_list($shopify_api_name_arr = array(), $shopify_url_param_array = [], $type = '', $shopify_is_object = 1) {
//        $shopinfo = $this->current_store_obj;
        $store_name = "vasucls.myshopify.com";
        $password = "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";
        $shopify_url_array = array_merge(array('/admin/api/2021-07'), $shopify_api_name_arr);
        $shopify_main_url = implode('/', $shopify_url_array) . '.json';
//           generate_log("collection",$shopify_main_url  . "url");
        $shopify_data_list = $this->cls_api_call('9875728c6b382b21f2b77786a7b11d06', $password, $store_name, $shopify_main_url, $shopify_url_param_array, $type);
        if ($shopify_is_object) {
            return json_decode($shopify_data_list['response']);
        } else {
            return json_decode($shopify_data_list['response'], TRUE);
        }
    }
    function cls_api_call($api_key , $password, $store, $shopify_endpoint, $query = array(),$type = '', $request_headers = array()) {
    $cls_shopify_url = "https://" . $api_key .":". $password ."@". $store.  $shopify_endpoint;
     if (!is_array($type) && !is_object($type)) {
        (array)$type;
    }
	if (!is_null($query) && in_array($type,array('GET','DELETE'))) $cls_shopify_url = $cls_shopify_url . "?" . http_build_query(array($query));
//	   generate_log("collection",$cls_shopify_url . "url");
	$curl = curl_init($cls_shopify_url);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
	$request_headers[] = "";
	if (!is_null($password)) $request_headers[] = "X-Shopify-Access-Token: " . $password;
	curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
	if ($type != 'GET' && in_array($type, array('POST', 'PUT'))) {
		if (is_array($query)) $query = http_build_query($query);
    		curl_setopt($curl, CURLOPT_POSTFIELDS,$query);
	}   
	$comeback = curl_exec($curl);
	$error_number = curl_errno($curl);
	$error_message = curl_error($curl);
	curl_close($curl);
	if ($error_number) {
		return $error_message;
	} else {
		$comeback = preg_split("/\r\n\r\n|\n\n|\r\r/",$comeback, 2);
		$headers = array();
		$header_data = explode("\n",$comeback[0]);
		$headers['status'] = $header_data[0]; 
		array_shift($header_data); 
		foreach($header_data as $part) {
			$h = explode(":", $part);
			$headers[trim($h[0])] = trim($h[1]);
		}
//		   generate_log("collection",json_encode( $comeback[1])  . "comeback");
		return array('headers' => $headers, 'response' => $comeback[1]);
	}
}
}

?>   