<!DOCTYPE html>
<html>
    <head>
        <title> Send an Email</title>
    </head>
    <body>
    <center>
        <h4 class="sent-notifi"></h4>
        <form id="myform">
            <h2>Send an Email</h2>
            <label>Name</label>
            <input id="name" type="text" placeholder="Enter Name">
            <br><br>
            <label>Email</label>
            <input id="email" type="text" placeholder="Enter Email">
            <br><br>
            <label>Subject</label>
            <input id="subject" type="text" placeholder="Enter Subject">
            <br><br>
            <p>massage</p>
            <textarea id="body" rows="5" placeholder="Type massage"></textarea>
            <br><br>
            <button type="button" onclick="sendemail()">Submit</button>
        </form>
    </center>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    
    <script type="text/javascript">
        function sendemail(){
            var name = $("#name");
            var email = $("#email");
            var subject = $("#subject");
            var body = $("#body");
            if(isNotEmpty(name) && isNotEmpty(email) && isNotEmpty(subject) && isNotEmpty(body)){
                $.ajax({
                    url: 'sendemailer.php',
                    method: 'POST',
                    datatype: 'json',
                    data: {
                        name: name.val(),
                        email: email.val(),
                        subject: subject.val(),
                        body: body.val()},
                    success: function(response){
                        $('#myform')[0].reset();
                        $('.sent-notifi').text("Email sent Successfully!");
                    }
                    
                });
            }
            
        }
        function isNotEmpty(caller){
            if(caller.val() == ""){
                caller.css('border','2px solid red');
                return false;
            }else{
                caller.css('border','2px solid blue');
                return true;
            }
        }
    </script>
</body>
</html>