
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Data table</title>
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="assets/js/data_table.js"></script>
    </head>
    <style>
        .data_table{
            text-align: center;
        }
        .dt-bootstrap4{
            padding: 10px;
        }
        .col-sm-12{
            padding: 20px;
        }
        .add_del_btn_div{
            display: flex;
            width: 100%;
        }
        .function_delete{
            display: flex;
            width: 50%;
            justify-content: center;
        }
        .clsmuldel{
            width: 50%;
        }
        .clscreate{
            display: flex;
            width: 50%;
            justify-content: center;
        }
        .cls_create{
            width: 50%;
        }
        .select-checkbox{
            /*display: flex;*/
            margin-top: 13px;
        }
    </style>
    <body class="theme-orange">
        <section class="content home">
            <div class="add_del_btn_div">
                <div class="function_delete"><a type="button" class="btn btn-danger btn-outline-dark clsmuldel"><i class="fa fa-trash-o" aria-hidden"true"></i></a></div>
                <div class="clscreate"><a type="button" class="btn  btn-raised btn-success waves-effect cls_create" href='multistep_form.php'>+</a></div>
            </div>
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr class="data_table">
                        <th></th>
                        <th>Order number</th>
                        <th>Remain Amount</th>
                        <th>Date</th>
                        <th>Client Name</th>
                        <th>Client Contact</th>
                        <th>Total amount</th>
                        <th>Client Image</th>
                    </tr>
                </thead>
                <tbody class="data_table">
                </tbody> 
                <tfoot>
                    <tr class="data_table">
                        <th></th>
                        <th>Order number</th>
                        <th>Remain Amount</th>
                        <th>Date</th>
                        <th>Client Name</th>
                        <th>Client Contact</th>
                        <th>Total amount</th>
                        <th>Client Image</th>
                    </tr>
                </tfoot>
            </table>
        </section>
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    </form>
</body>
</html>