<?php
include 'img_function.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>Happy Event | Event planner | Birthday Organizer</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" />
        <link rel="stylesheet" href="assets/plugins/morrisjs/morris.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <link rel="stylesheet" href="assets/css/datatable_style.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="assets/js/data_table.js"></script>
    </head>
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore CodeLock...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>
        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>
        <section class="content home">
            <div class="add_del_btn_div">
                <div class="function_delete"><a type="button" class="btn btn-danger btn-outline-dark clsmuldel"><i class="fa fa-trash-o" aria-hidden"true"></i></a></div>
                <div class="clscreate"><a type="button" class="btn  btn-raised btn-success waves-effect cls_create" href='multistep_form.php'>+</a></div>
            </div>
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr class="data_table">
                        <th></th>
                        <th></th>
                        <th>Order Number</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Client Number</th>
                        <th>Advance Amount</th>
                        <th>Remain Amount</th>
                        <th>Total Amount</th>
                        <th>Client Image</th>
                    </tr>
                </thead>
                <tbody class="data_table">
                </tbody> 
                <tfoot>
                    <tr class="data_table">
                        <th></th>
                        <th></th>
                        <th>Order Number</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Client Number</th>
                        <th>Advance Amount</th>
                        <th>Remain Amount</th>
                        <th>Total Amount</th>
                        <th>Client Image</th>
                    </tr>
                </tfoot>
            </table>
        </section>
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/bundles/vendorscripts.bundle.js"></script>
        <script src="assets/bundles/knob.bundle.js"></script>
        <script src="assets/bundles/mainscripts.bundle.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    </body>
</html>