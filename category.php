<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="decoration" content="event management surat">
    <meta name="decoration" content="Baby shower decoration surat">
    <meta name="decoration" content="theme decoration surat">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title>Bethany - Wedding & Event Planner</title>
    <link href="css/bootstrap.min.css" rel=stylesheet>
    <link href="css/animate.css" rel="stylesheet">
  
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script src="js/header-footer.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-144098545-1');
    </script>
</head>
<body>

	  <div w3-include-html="header.html"></div> 
        <script>
            includeHTML();
        </script>
	<section class="baby_shower">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Baby Shower</h2>
				</div>
			</div>
			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/hpimg/b1.jpg" alt="baby1" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
							<img src="images/ballon/about/b2.jpg" alt="baby2" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/hpimg/babyr1.jpg" alt="baby3" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/1.jpeg" alt="baby4" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/hpimg/bn5.jpg" alt="baby5" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/hpimg/sn1.jpg" alt="baby6" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/hpimg/bn2.jpg" alt="baby7" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/b8.jpg" alt="baby8" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/b9.jpg" alt="baby9" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/b10.jpg" alt="baby10" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/b11.jpg" alt="baby11" class="event_img" title="babyshower">
					</div>
					<div class="baby25">
								<img src="images/hpimg/bn4.jpg" alt="baby12" class="event_img" title="babyshower">
					</div>
						<div class="baby25">
								<img src="images/hpimg/bs2006.webp" alt="baby13" class="event_img" title="babyshower">
					</div>
						<div class="baby25">
								<img src="images/hpimg/bs4006.webp" alt="baby14" class="event_img" title="babyshower">
					</div>
						<div class="baby25">
								<img src="images/hpimg/bn9.webp" alt="baby15" class="event_img" title="babyshower">
					</div>
					   <div class="baby25">
								<img src="images/hpimg/bs1006.webp" alt="baby16" class="event_img" title="babyshower">
					</div>  

				</div>
			</div>
	</section>
	
	<section class="baby_shower rak">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Baby Welcome </h2>
				</div>
			</div>
			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/ballon/about/w1.jpg" alt="birthdy1" class="event_img">
					</div>
					<div class="baby25">
							<img src="images/ballon/about/w2.jpg" alt="birthdy2" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/we8.jpg" alt="birthdy3" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/w4.jpg" alt="birthdy4" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/g15.jpg" alt="birthdy5" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/we6.webp" alt="birthdy6" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/we4.webp" alt="birthdy7" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/w8.jpg" alt="birthdy8" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/g18.jpg" alt="birthdy9" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/we5.webp" alt="birthdy10" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/g20.jpg" alt="birthdy11" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/we3.webp" alt="birthdy12" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/we1.webp" alt="birthdy13" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/we2.webp" alt="birthdy14" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/g25.webp" alt="birthdy15" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/g24.webp" alt="birthdy16" class="event_img">
					</div>
				</div>
			</div>
	</section>
	
	<section class="baby_shower rak">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Birthday Decoration</h2>
				</div>
			</div>
			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/ballon/about/r1.jpg" alt="birthdy1" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
							<img src="images/ballon/about/r2.jpg" alt="birthdy2" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r3.png" alt="birthdy3" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r4.png" alt="birthdy4" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r5.png" alt="birthdy5" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r6.png" alt="birthdy6" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r7.png" alt="birthdy7" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r8.jpg" alt="birthdy8" class="event_img" title="birthday decoration"> 
					</div>
					<div class="baby25">
								<img src="images/ballon/about/bi3.webp" alt="birthdy9" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r10.jpg" alt="birthdy10" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/bi1.webp" alt="birthdy11" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r12.jpg" alt="birthdy12" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r13.jpg" alt="birthdy13" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r14.jpg" alt="birthdy14" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r15.jpg" alt="birthdy15" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r16.jpg" alt="birthdy16" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r17.jpg" alt="birthdy17" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r18.jpg" alt="birthdy18" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/bi2.webp" alt="birthdy19" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r20.jpg" alt="birthdy20" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/r21.jpg" alt="birthdy21" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/rn1.jpg" alt="birthdy23" class="event_img" title="birthday decoration">
					</div>
						<div class="baby25">
								<img src="images/ballon/about/bi4.webp" alt="birthdy22" class="event_img" title="birthday decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/bi5.webp" alt="birthdy23" class="event_img" title="birthday decoration">
					</div>
				</div>
			</div>
	</section>
	
	<section class="baby_shower">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Naming Ceremony </h2>
				</div>
			</div>
			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/baby/s1.jpg" alt="name1" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
							<img src="images/baby/s5.jpeg" alt="name2" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/baby/s2.jpg" alt="name3" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/c4.jpg" alt="name4" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/baby/s4.jpg" alt="name6" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/hpimg/sn2.jpg" alt="name7" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/hpimg/sn3.jpg" alt="name8" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/hpimg/wn2.jpg" alt="name9" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/bn2.jpg" alt="name10" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/baby/s7.jpg" alt="name11" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/c12.jpg" alt="name12" class="event_img" title="name ceremoney">
					</div>
				
					<div class="baby25">
								<img src="images/ballon/about/c14.jpg" alt="name14" class="event_img" title="name ceremoney">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/name1.jpg" alt="name15" class="event_img" title="name ceremoney">
					</div>
				</div>
			</div>
	</section>
	
		<section class="baby_shower rak">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Room Decoration</h2>
				</div>
			</div>
			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/ballon/about/d1.jpg" alt="room1" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
							<img src="images/baby/d1.jpg" alt="room2" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/d2.jpg" alt="room3" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/rm3.webp" alt="room4" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/d4.jpeg" alt="room5" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/rm1.webp" alt="room6" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/dn1.jpg" alt="room7" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/dn2.jpg" alt="room8" class="event_img" title="room decoration"> 
					</div>
					<div class="baby25">
								<img src="images/baby/d7.jpg" alt="room9" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/d8.jpg" alt="room10" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/d11.jpg" alt="room11" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/dn3.jpg" alt="room12" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/d13.jpg" alt="room13" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/rm5.webp" alt="room14" class="event_img" title="room decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/d15.jpg" alt="room15" class="event_img" title="room decoration"> 
					</div>
					<div class="baby25">
								<img src="images/ballon/about/rm2.webp" alt="room16" class="event_img" title="room decoration">
					</div>
				</div>
			</div>
	</section>
	
	<section class="baby_shower rak">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Theme Decoration</h2>
				</div>
			</div>

			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/hpimg/hn1.jpg" alt="theme1" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
							<img src="images/hpimg/hn2.jpg" alt="theme2" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn3.jpg" alt="theme3" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn4.jpg" alt="theme4" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn5.jpg" alt="theme7" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn6.jpg" alt="theme8" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn7r.webp" alt="theme9" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn8.jpg" alt="theme10" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn9r.webp" alt="theme11" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn10.png" alt="theme12" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn11.jpg" alt="theme13" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn12.jpg" alt="theme14" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn13.jpg" alt="theme15" class="event_img" title="them decoration">
					</div>
					
					<div class="baby25">
								<img src="images/hpimg/hn15.jpg" alt="theme17" class="event_img" title="them decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/hn19.jpg" alt="theme18" class="event_img" title="them decoration"> 
					</div>
				</div>
			</div>
	</section>
	
		<section class="baby_shower rak">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Haldi Ceremony </h2>
				</div>
			</div>

			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/hpimg/an1.jpg" alt="healdy1" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
							<img src="images/ballon/about/ha9.jpg" alt="healdy2" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/h3.jpg" alt="healdy3" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/h4.jpg" alt="healdy4" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ha3.jpg" alt="healdy5" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/h6.jpg" alt="healdy6" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ha4.jpg" alt="healdy7" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/hpimg/an4.jpg" alt="healdy8" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ha6.jpg" alt="healdy9" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/h10.jpg" alt="healdy10" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/hpimg/an2.jpg" alt="healdy11" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/h12.jpg" alt="healdy12" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/h13.jpg" alt="healdy13" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/hpimg/an3.jpg" alt="healdy14" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/h15.jpg" alt="healdy15" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ha5.jpg" alt="healdy16" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ha8.jpeg" alt="healdy17" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ha7.jpeg" alt="healdy18" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ha10.jpg" alt="healdy19" class="event_img" title="haldi ceremony">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/h20.jpg" alt="healdy20" class="event_img" title="haldi ceremony">
					</div>
					

				</div>
			</div>
	</section>
	
	<section class="baby_shower rak">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Bride to be Decoration</h2>
				</div>
			</div>

			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/ballon/about/e1.jpg" alt="bride1" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
							<img src="images/baby/b2.jpg" alt="bride2" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/b1.jpg" alt="bride3" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/b3.jpg" alt="bride4" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e5.jpg" alt="bride5" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/b6.jpg" alt="bride6" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e7.jpg" alt="bride7" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/b7.jpg" alt="bride8" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e9.jpg" alt="bride9" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/b8.jpg" alt="bride10" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e11.jpg" alt="bride11" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e12.jpg" alt="bride12" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e13.jpg" alt="bride13" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/b9.jpg" alt="bride14" class="event_img" title="bride decoration">
					</div>
					
					<div class="baby25">
								<img src="images/baby/b10.jpg" alt="bride16" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e17.jpg" alt="bride17" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e18.jpg" alt="bride18" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e19.jpg" alt="bride19" class="event_img" title="bride decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/e20r.webp" alt="bride20" class="event_img" title="bride decoration">
					</div>
					

				</div>
			</div>
	</section>
	
		<section class="baby_shower rak">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Engagement Decoration </h2>
				</div>
			</div>

			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/baby/e1.jpg" alt="enagedge1" class="event_img" title="engagement">
					</div>
					<div class="baby25">
							<img src="images/ballon/about/f2.jpg" alt="enagedge2" class="event_img"  title="engagement">
					</div>
					<div class="baby25">
								<img src="images/hpimg/IMG-20210714-WA0017.jpg" alt="enagedge3" class="event_img"  title="engagement">
					</div>
					<div class="baby25">
								<img src="images/baby/e4.jpg" alt="enagedge4" class="event_img"  title="engagement">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/f5.jpg" alt="enagedge5" class="event_img"  title="engagement">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/f6.jpg" alt="enagedge6" class="event_img"  title="engagement">
					</div>
					<div class="baby25">
								<img src="images/baby/e5.jpg" alt="enagedge7" class="event_img"  title="engagement">
					</div>
					<div class="baby25">
								<img src="images/baby/e2.jpg" alt="enagedge8" class="event_img" title="engagement"
								>
					</div>
					<div class="baby25">
								<img src="images/ballon/about/f9.jpg" alt="enagedge9" class="event_img" title="engagement">
					</div>
					<div class="baby25">
								<img src="images/baby/e6.jpg" alt="enagedge10" class="event_img" title="engagement">
					</div>
					<div class="baby25">
								<img src="images/hpimg/en1.jpg" alt="enagedge11" class="event_img" title="engagement">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/f12.jpg" alt="enagedge12" class="event_img" title="engagement">
					</div>
					<div class="baby25">
								<img src="images/hpimg/en2.jpg" alt="enagedge13" class="event_img" title="engagement">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/f14.jpg" alt="enagedge14" class="event_img" title="engagement">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/en1.webp" alt="enagedge15" class="event_img"  title="engagement">
					</div>
				</div>
			</div>
	</section>
	
	<section class="baby_shower">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Shop Inauguration </h2>
				</div>
			</div>

			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/ballon/about/shopr1.jpg" alt="shop1" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
							<img src="images/baby/h1.jpg" alt="shop2" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/h2.jpg" alt="shop3" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/bn1.jpg" alt="shop4" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/shopr2.jpg" alt="shop5" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/s6.jpg" alt="shop6" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/bn3.jpg" alt="shop7" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/h5.jpg" alt="shop8" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/s9.jpg" alt="shop9" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/s10.jpg" alt="shop10" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/s11.jpg" alt="shop11" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/baby/h6.jpeg" alt="shop12" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/sh2.webp" alt="shop13" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/on1.jpg" alt="shop14" class="event_img" title="shop decoration">
					</div>
					<div class="baby25">
								<img src="images/hpimg/on2.jpg" alt="shop15" class="event_img" title="shop decoration">
					</div>
			</div>
	</section>
	
		<section class="baby_shower rak">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Corporate Event</h2>
				</div>
			</div>

			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/hpimg/cn1.jpg" alt="corporate1" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
							<img src="images/hpimg/cn2.jpeg" alt="corporate2" class="event_img"  title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/hpimg/cn3.webp" alt="corporate3" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/q4.jpg" alt="corporate4" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/hpimg/cn4.jpg" alt="corporate5" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/q6.jpg" alt="corporate6" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/co2.webp" alt="corporate7" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/q8.jpg" alt="corporate8" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/q9.jpg" alt="corporate9" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/hpimg/cn5.jpg" alt="corporate10" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/co1.webp" alt="corporate11" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/hpimg/cn6.jpg" alt="corporate12" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/hpimg/cn7.jpg" alt="corporate13" class="event_img" title="corporate event">
					</div>
					<div class="baby25">
								<img src="images/hpimg/cn8.jpg" alt="corporate13" class="event_img" title="corporate event">
					</div>
					
					
					

				</div>
			</div>
	</section>
	
	<section class="baby_shower">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Surprise Planning</h2>
				</div>
			</div>

			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						<img src="images/baby/r1.jpg" alt="surprice1" class="event_img">
					</div>
					<div class="baby25">
							<img src="images/ballon/about/r10.jpg" alt="surprice2" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ma6.webp" alt="surprice3" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/r6.jpg" alt="surprice4" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/Y5.jpg" alt="surprice5" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ma2.webp" alt="surprice6" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ma1.webp" alt="surprice7" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/Y8.jpg" alt="surprice8" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/Y9.jpg" alt="surprice9" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ma3.webp" alt="surprice10" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/Y11.jpg" alt="surprice11" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/Y12.jpg" alt="surprice12" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/Y13.jpg" alt="surprice13" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/ma4.webp" alt="surprice14" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/r4.jpg" alt="surprice15" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/hb1006.jpg" alt="surprice16" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/baby/r5.jpg" alt="surprice17" class="event_img">
					</div>
					<div class="baby25">
								<img src="images/ballon/about/en1.jpg" alt="surprice18" class="event_img">
					</div>
				</div>
			</div>
	</section>
	 <div w3-include-html="footer.html"></div> 
        <script>
            footerHTML();
        </script>

         <script src="js/plugins/jquery-3.5.1.min.js"></script>
    <script src="js/plugins/bootstrap.min.js"></script>
        <script src="js/script.js"></script>

</body>
</html>