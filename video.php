<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from duruthemes.com/demo/html/bethany/demo2/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Oct 2021 05:51:37 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title>Bethany - Wedding & Event Planner</title>
    <link href="css/bootstrap.min.css" rel=stylesheet>
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/themify-icons.css" rel="stylesheet">
    <link href="modules/slick/slick.css" rel="stylesheet">
    <link href="modules/slick/slick-theme.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
    <link href="modules/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="modules/youtubepopup/YouTubePopUp.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <!--<script src="js/plugins/jquery-3.5.1.min.js"></script>-->
     <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
     <script src="assets/js/img_ajax1.js"></script>
    <script src="js/plugins/bootstrap.min.js"></script>
    <script src="js/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="js/plugins/jquery.isotope.v3.0.2.js"></script>
    <script src="js/plugins/modernizr-2.6.2.min.js"></script>
    <script src="js/plugins/jquery.waypoints.min.js"></script>
    <script src="modules/owl-carousel/owl.carousel.min.js"></script>
    <script src="modules/slick/slick.js"></script>
    <script src="modules/magnific-popup/jquery.magnific-popup.js"></script>
    <script src="modules/masonry/masonry.pkgd.min.js"></script>
    <script src="modules/youtubepopup/YouTubePopUp.js"></script>
    <script src="js/script.js"></script>
     <script src="js/header-footer.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-144098545-1');
    </script>
</head>

<body>
    <!-- Preloader -->
    <div id="bethany-page-loading" class="bethany-pageloading">
        <div class="bethany-pageloading-inner"> 
            <img src="images/logo-dark.png" class="logo" alt=""> 
        </div>
    </div>
    <!-- Navigation Bar -->
     <div w3-include-html="happyheader.php"></div> 
        <script>
            includeHTML();
        </script>
    <!-- videos -->
    <section class="video_gallary container2">
               <div class="baby_contain">
                <div class="baby_title">
                    <h2 class="title">Video Gallery</h2>
                </div>
            </div>
            <div class="custon_videos ">
                    <div class="video_flex videoGallerySetData">
<!--                            <div class="video50">
                                <div class="evenr">
                                        <iframe width="560" height="400" src="https://www.youtube.com/embed/VYS_1_8unIk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="video50">
                                    <div class="evenr">
                                       <iframe width="560" height="400" src="https://www.youtube.com/embed/bJvlmjAqhcg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                              <div class="video50">
                                    <div class="evenr">
                                     <iframe width="560" height="400" src="https://www.youtube.com/embed/mUfctXwcVSk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                              <div class="video50">
                                    <div class="evenr">
                                      <iframe width="560" height="400" src="https://www.youtube.com/embed/Sp8TUp_bPM0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                              <div class="video50">
                                    <div class="evenr">
                                       <iframe width="560" height="400" src="https://www.youtube.com/embed/F2Axyamtvwo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                              <div class="video50">
                                    <div class="evenr">
                                      <iframe width="560" height="400" src="https://www.youtube.com/embed/lQa2gVVMRHg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>

                                <div class="video50">
                                    <div class="evenr">
                                    <iframe width="560" height="400" src="https://www.youtube.com/embed/K5mjcatw0TU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                                <div class="video50">
                                    <div class="evenr">
                                      <iframe width="560" height="400" src="https://www.youtube.com/embed/ab3Uh8iA8VM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                                <div class="video50">
                                    <div class="evenr">
                                     <iframe width="560" height="400" src="https://www.youtube.com/embed/QKnz-VzVsxY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                                <div class="video50">
                                    <div class="evenr">
                                    <iframe width="560" height="400" src="https://www.youtube.com/embed/-g93DnEb9-k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>

                              <div class="video50 down">
                                    <div class="evenr">
                                  <iframe width="560" height="400" src="https://www.youtube.com/embed/gzTn8_zxB3w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                              <div class="video50 down">
                                    <div class="evenr">
                                    <iframe width="560" height="400" src="https://www.youtube.com/embed/y9xXRguR3Ug" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>-->

                    </div>
            </div>
    </section>
  
   
    <!-- Footer -->
      <div w3-include-html="footer.html"></div> 
        <script>
            footerHTML();
        </script>
    <!-- toTop --><a href="index.html#" class="totop">TOP</a>
    <!-- jQuery -->
   
</body>

<!-- Mirrored from duruthemes.com/demo/html/bethany/demo2/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Oct 2021 05:51:53 GMT -->
</html>
<script>
    $(document).ready(function () {
            set_video_gallery_page();
    });
</script>