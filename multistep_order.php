<?php
include 'img_function.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>Happy Event | Event planner | Birthday Organizer</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" />
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/morrisjs/morris.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Custom Css -->
        <link href="assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <link rel="stylesheet" href="assets/css/multistep_order.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.js"></script>
        <script src="assets/js/img_ajax1.js"></script>
    </head>
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore CodeLock...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>


        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>

        <section class="content home">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Order
                            <small class="text-muted">Welcome to Happy Event</small>
                        </h2>
                        <center><span class="cls_order_msg"></span></center>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-sm-12 col-md-12 col-lg-12 table_main_div">
                        <div class="card">
                            <div class="header">
                                <h2><b>Dashboard</b></h2>
                            </div>
                            <div class="cls_function_class">
                                <div class="fix_div">
                                    <div class="cls_search_div">
                                        <input type="search" class="clssearch" data-table="order-table" placeholder="search at least 3 characters"/>
                                    </div>
                                    <div class="filter_div"><input type="date" id="filter_date" name="date" placeholder="Filter By Date" />
                                        <div class="show_order_fltr_div">
                                            <select class='show-tick show_order_data_filter'  name='clsorder_data_status_fltr'>
                                                <option value='' selected>Order Status</option>
                                                <option value='0' name='clsorder_data_status_fltr'>Panding</option>
                                                <option value='1' name='clsorder_data_status_fltr'>Complete</option>
                                                <option value='2' name='clsorder_data_status_fltr'>Cancel</option>
                                                <option value='3' name='clsorder_data_status_fltr'>Done</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="add_del_btn_div">
                                        <div class="function_delete"><a type="button" class="btn btn-danger btn-outline-dark clsmuldel"><i class="fa fa-trash-o" aria-hidden"true"></i></a></div>
                                        <div class="clscreate"><a type="button" class="btn  btn-raised btn-success waves-effect cls_create" href='multistep_form.php'>+</a></div>
                                    </div>
                                </div>
                                    <div class="body table-responsive members_profiles">
                                        <table class="table table-hover" data-listing="true">
                                            <thead class="clsthead">
                                                <tr>
                                                    <th></th>
                                                    <th style="width:60px;">Id</th>
                                                    <th>Order Number</th>
                                                    <th>Date</th>
                                                    <th class="clsel">Time</th>
                                                    <th class="clsel">Client Number</th>
                                                    <th class="clsel">Advance Amount</th>
                                                    <th class="clsel">Remain Amount</th>
                                                    <th class="clsel">Total Amount</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="order_tbody">

                                            </tbody>
                                        </table>
                                        <div class="clsnav">
                                            <nav class="clsnav">
                                                <ul class="pagination pagination-lg pagination_html"></ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </section>
                    <script src="assets/bundles/libscripts.bundle.js"></script>
                    <script src="assets/bundles/vendorscripts.bundle.js"></script>
                    <script src="assets/bundles/knob.bundle.js"></script>
                    <script src="assets/bundles/mainscripts.bundle.js"></script>
                    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>
                    <script src="assets/js/pages/ui/dialogs.js"></script>
                    <script src="assets/bundles/mainscripts.bundle.js"></script>
                    <!--<script src="assets/js/pages/index.js"></script>-->
                    </body>
                    </html>
                    <script>
                        $(document).ready(function () {
                            order_details_data();
                            scroll_fixed_div();
                        });
                    </script>