<?php
include 'function1.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:index.php");
}
?>
﻿<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>Dashboard</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/xs/avatar7.jpg" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" />
        <link rel="stylesheet" href="assets/plugins/morrisjs/morris.css" />
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="../nexa/assets/js/ajax.js"></script>
    </head>

    <style>
        .theme-orange .user-info{
            display: flex;
            padding-left: 1px;
        }
        .clsnav{
            text-align: center;
            justify-content: center;
            display: flex;
            cursor:pointer;
        }
        .page_center{
            display: flex;
        }
        .error-msg{
            color: red;
            font-size: 25px;
            float: right;
        }
        .clsdiv{
            width: 80%;
            text-align: center;
        }
        .clssearch{
            text-align: center;
            width: 95%;
            height: 35px;
            margin-left: 40px;
        }
        .clsclass{
            display: flex;
        }
        .theme-orange .muldel {
            left: 32px;
            opacity: 1;
            margin-top: -4px;
        }
        .clsmuldel{
            margin-left: 30px;
            width: 97px;
            height: 35px;
            align-items: center;
            justify-content: center;
            display: flex;
        }
        .clscreate{
            margin-left: 16px;
            height: 35px;
            align-items: center;
            justify-content: center;
            display: flex;
        }
        .material-icons{
            margin-top: 6px;
            position: absolute;
            margin-left: 76.5%;
        }
        .clsview{
            width: 69px;
        }
        @media screen and (max-width: 400px) {
            .clsmuldel {
                width: 68px;
            }
            .material-icons {
                display: none;
            }
            .clssearch{
                margin-left: 10px;
            }
            .clscreate{
                margin-right: 10px;
            }
            .clsel {
                display: none;
            }
            .clsme{
                display: none;
            }
            .clsdelete {
                margin-top: -4px;
                margin-bottom: 4px;
                width: 67px;
            }
        }
    </style>
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/logo.svg" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore CodeLock...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>


        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>

        <section class="content home">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Dashboard
                            <small class="text-muted">Welcome to CodeLock Solution</small>
                        </h2>
                        <!--<center><span class="error-msg clsdel clsinsert"></span></center>-->
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> Code Lock</a></li>
                            <li class="breadcrumb-item active">Dashboard </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h2><b>Dashboard</b></h2>
                            </div>
                            <div class="clsclass">
                                <div class="clsdiv">
                                    <i class="material-icons">search</i>
                                    <input type="search" class="clssearch" data-table="order-table" placeholder="search at least 3 characters"/>
                                </div>
                                <div class="create"><a type="button" class="btn btn-danger btn-outline-dark clsmuldel">Delete</a></div>
                                <div class="clscreate"><a type="button" class="btn  btn-raised btn-success waves-effect clscreate" href='reform.php'>Add Record</a></div>
                            </div>
                            <div class="body table-responsive members_profiles">
                                <table class="table table-hover" data-listing="true">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th style="width:60px;">Id</th>
                                            <th>Profile Image</th>
                                            <th>User Name</th>
                                            <th class="clsel">Emails</th>
                                            <th class="clsel">Phone Number</th>
                                            <th class="clsel">Books</th>
                                            <th class="clsel">About</th>
                                            <th class="clsel">Role</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="clstbody">

                                    </tbody>
                                </table>
                                <div class="clsnav">
                                    <nav class="clsnav">
                                        <ul class="pagination pagination-lg pagination_html"></ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="assets/bundles/vendorscripts.bundle.js"></script>
        <script src="assets/bundles/knob.bundle.js"></script>
        <script src="assets/bundles/mainscripts.bundle.js"></script>
        <script src="assets/js/pages/index.js"></script>
    </body>
</html>
