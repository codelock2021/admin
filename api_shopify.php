
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>API Learning</title>
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link href="assets/plugins/waitme/waitMe.css" rel="stylesheet" />
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link  rel="stylesheet" href="assets/css/main.css">
        <link  rel="stylesheet" href="assets/css/imggallery.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="assets/js/img_ajax2.js"></script>
        <script src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5-stable/tinymce.min.js"></script>
    </head>
    <style>
        .card{
            width: 60%;
            margin-left: 20%;
        }
        lable{
            font-size: 25px;
        }
        .title{
            text-align: center;
            margin-top: 10px;
        }
        .title_input{
            width: 80%;
            height: 41px;
        }
        .pro_image_div{
            display: flex;
            margin-top: 10px;
            align-items: center;
            justify-content: center;
        }
        .image_input{
            margin-left: 5px;
            width: 929px;
        }
        .pro_descri_div{
            margin-top: 10px;
            display: flex;
            align-items: center;
        }
        #basic-example{
            margin-left: 5px;
        }
        .save_btn{
            display: flex;
            justify-content: center;
        }
        .errorMsg{
            font-size: 25px;
            color: red;
        }
        .errorImg{
            font-size: 25px;
            color: red;
        }
        .errordes{
            font-size: 25px;
            color: red;
        }
    </style>
    <body class="theme-orange">
        <div class="page-loader-wrapper" style="display: none">
            <div class="loader">        
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div><!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>
        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>API Data</h2>
                    </div>
                </div>
            </div>
            <center><span class="errorMsg"></span></center>
            <center><span class="errorImg"></span></center>
            <center><span class="errordes"></span></center>
            <div class="card">
                <form name="iform" class="iform" enctype="multipart/form-data" method="POST">
                    <div class="title">
                        <lable class="title_lable">Title:</lable>
                        <input class="title_input" type="text" name="title_name" placeholder="Enter Product Title">
                    </div>
                    <div class="pro_image_div">
                        <lable class="image_lable">Images:</lable>
<!--                        <div class="add_images_div"> 
                            <input type="file" name="image_input" class="image_input">
                        </div>-->
                    </div>
                    <div class="pro_descri_div">
                        <lable class="descri_lable">Description:</lable>
                        <textarea id="basic-example"></textarea>
                    </div>
                    <div class="save_btn"><button class="save">Save</button></div>
                </form>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
        <script src="assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js --> 
        <!-- Bootstrap Material Datetime Picker Plugin Js --> 
        <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
        <script src="assets/js/pages/forms/basic-form-elements.js"></script> 
    </form>
</body>
</html>