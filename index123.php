<html>
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
        body, html {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  margin: 0;
  padding: 0;
  overflow: hidden;    
  background-color:#000;
}

.squares li{
  position: absolute;
  display: block;
  list-style: none;
  background: rgba(255, 255, 255, 0.1);
  animation: animate 30s linear infinite;
  bottom: -165px;
  width: 25px;
  height: 25px;
}

.squares li:nth-child(1){
  left: 26%;
  width: 90px;
  height: 90px;
  animation-delay: 0s;
}

.squares li:nth-child(2){
  left: 8%;
  width: 5px;
  height: 5px;
  animation-delay: 5s;
  animation-duration: 5s;
}

.squares li:nth-child(3){
  left: 10%;
  width: 15px;
  height: 15px;
  animation-delay: 10s;
}

.squares li:nth-child(4){
  left: 20%;
  width: 20px;
  height: 20px;
  animation-delay: 6s;
  animation-duration: 40s;
}

.squares li:nth-child(5){
  left: 35%;
  width: 80px;
  height: 80px;
  animation-delay: 2s;
}

.squares li:nth-child(6){
  left: 40%;
  width: 40px;
  height: 40px;
  animation-delay: 10s;
}

.squares li:nth-child(7){
  left: 55%;
  width: 200px;
  height: 200px;
}

.squares li:nth-child(8){
  left: 65%;
  width: 15px;
  height: 15px;
  animation-duration: 35s;
}

.squares li:nth-child(9){
  left: 70%;
  width: 25px;
  height: 25px;
  animation-delay: 20s;
  animation-duration: 20s;
}

.squares li:nth-child(10){
  left: 85%;
  width: 100px;
  height: 100px;
  animation-delay: 1s;
  animation-duration: 15s;
}

@keyframes animate {
  0% {
    transform: translateY(0) rotate(0deg);
    opacity: 1;
    border-radius: 30%;
    -webkit-border-radius: 30%;
    -moz-border-radius: 30%;
  }

  100% {
    transform: translateY(-1000px) rotate(600deg);
    opacity: 0;
    border-radius: 50%;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;  }
}

#middle {
  position: absolute;
  top: 80%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

#middle h1 {
  color: #FFFFFF;
  font-size: 3.80em;
  font-family: Times New Roman;
  text-shadow: 0px 1.5px 1.5px rgba(0, 0, 0, 0.3);
  line-height: 1;
  text-align: left;
}



#middle h3 {
  color: #FFFFFF;
  font-size: 1.20em;
  display: inline-block;
  margin-right: 0.9em;
}

#middle i {
  margin-left: 0.9em;
}

#middle a {
  color: #FFFFFF;
  text-decoration: none;
}

#middle a:hover {
  color: #FFFFFF;
  text-decoration: none;
  text-shadow: 0px 2.2px 2.2px rgba(0, 0, 0, 0.4);
}

#twitter:hover {
  color: #00acee;
}

#medium:hover {
  color: #000000;
}

#dribbble:hover {
  color: #ea4c89;
}

#codepen:hover {
  color: #000000;
}

#middle img {
  padding: 0.5em;
}

#middle img a {
  text-decoration: none;
}

#middle img:hover {
  text-decoration: none;
  opacity: 0.8;
}
.logo{
  flex: 0 1 20vw;
    text-align: center;
}
.logo-favicon{
    display:none;
}
ul {
    list-style: none;
}

ul li {
    margin:5px 20px;
    float:left;
}

.wp-icon {
  width: 50px;
  height: 50px;
  border-radius: 50%;
  text-align: center;
  line-height: 50px;
  vertical-align: middle;
  color: #fff;
  margin-right: 5px;
}

.fa-facebook-f {
  background: #3B5998;
}

.fa-linkedin {
  background: #0077B5;
}

.fa-twitter {
  background: #1DA1F2;
}
.fa-instagram {
  background: #d6249f;
  background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%);
  box-shadow: 0px 3px 10px rgba(0,0,0,.25);
}
.fa-google-plus {
  background: #D04338;
}
.fa-youtube {
  background: #FF0000;
}

.fa-pinterest {
  background: #BD081C;
}
.social-icons-cls{
    margin-left: 40%;
    margin-top: 5%;
}
@media screen and (max-width: 480px) {
  .social-icons-cls{
    margin-left: 0;
    margin-top: 0;
    width:100%;
  }
  .social-icons-cls ul{
          width: 80%;
    margin-left: 10%;
  }
  .logo-favicon{
    display:block;
    margin-left:10%;
   
  }
  .logo-favicon .logo-cls{
       height: auto;
    width: 80%;
  }
  .logo{
    display:none;
  }
  #middle{
    position: unset;
    top: 80%;
    left: 0;
    transform: unset;
    margin-top: 20%;
  }
  #middle h1{
      text-align:unset;
  }
  
}
    
</style> 
    </head>
    <body>
    <div id="testing">
        <ul class="squares">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
       <div  class="logo"><img class="logo-cls" src="images/happyevent.png"></div>
       <div  class="logo-favicon"><img class="logo-cls" src="images/favicon.png"></div>
       <!--<div  class="logo-mobile-view"><img class="logo-section-cls" src="images/happyevent-mobile.png"></div>           -->
        <div id="middle">
            <h1>COMING SOON!</h1>
        </div><!-- /#middle -->
        <div class="social-icons-cls">
        <ul>
                <li><a href="https://www.facebook.com/"><i class="fa wp-icon fa-facebook-f fa-lg"></a></i></li>
                <li><a href="https://www.instagram.com/"><i class="fa wp-icon fa-instagram fa-lg"></a></i></li>
                <li><a href=""><i class="fa wp-icon fa-google-plus fa-lg"></a></i></li>
        </ul> 
       </div> 
    </div>   
    </body>
   
</html>
