<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from duruthemes.com/demo/html/bethany/demo2/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Oct 2021 05:51:53 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="decoration" content="event management surat">
    <meta name="decoration" content="Baby shower decoration surat">
    <meta name="decoration" content="theme decoration surat">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title>Bethany - Wedding & Event Planner</title>
    <link href="css/bootstrap.min.css" rel=stylesheet>
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/themify-icons.css" rel="stylesheet">
    <link href="modules/slick/slick.css" rel="stylesheet">
    <link href="modules/slick/slick-theme.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
    <link href="modules/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="modules/youtubepopup/YouTubePopUp.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/header-footer.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-144098545-1');
    </script>
</head>

<body>
   <!-- Preloader -->
    <div id="bethany-page-loading" class="bethany-pageloading">
        <div class="bethany-pageloading-inner"> 
            <img src="images/logo-dark.png" class="logo" alt=""> 
        </div>
    </div>
    <!-- Navigation Bar -->
     <div w3-include-html="happyheader.php"></div> 
        <script>
            includeHTML();
        </script>
  
    <!-- Header Banner -->
       <!-- Blog -->
 <section class="baby_shower">
            <div class="baby_contain">
                <div class="baby_title">
                    <h2 class="title">Photo Gallery</h2>
                </div>
            </div>

            <div class="baby_all">
                <div class="baby_flex photogallerryDiv">
<!--                    <div class="baby25">
                        <img src="images/ballon/about/d1.jpg" alt="gallary1" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                            <img src="images/hpimg/we1.webp" alt="gallary2" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                               <img src="images/ballon/about/w14.jpg" alt="gallary3" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/hpimg/round.jpg" alt="gallary4" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/ballon/about/r17.jpg" alt="gallary5" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/hpimg/wel2.jpg" alt="gallary6" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/hpimg/wel3.webp" alt="gallary7" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/hpimg/wel4.jpg" alt="gallary8" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/ballon/about/d9.jpg" alt="gallary9" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                               <img src="images/ballon/about/t9.jpg" alt="gallary10" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/hpimg/wel5.jpg" alt="gallary11" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/ballon/about/c14.jpg" alt="gallary12" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/hpimg/wel6.jpg" alt="gallary13" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                              <img src="images/ballon/about/h10.jpg" alt="gallary14" class="event_img" title="galary" >
                    </div>
                    <div class="baby25">
                                <img src="images/ballon/about/d15.jpg" alt="gallary15" class="event_img" title="galary">
                    </div>
                    <div class="baby25">
                                <img src="images/hpimg/wel7.jpg" alt="gallary16" class="event_img" title="galary">
                    </div>
                      <div class="baby25">
                               <img src="images/ballon/about/Y9.jpg" alt="gallary17" class="event_img" title="galary">
                    </div>
                      <div class="baby25">
                                <img src="images/ballon/about/e1.jpg" alt="gallary18" class="event_img" title="galary">
                    </div>
                      <div class="baby25">
                              <img src="images/ballon/about/q6.jpg" alt="gallary19" class="event_img" title="galary">
                    </div>
                      <div class="baby25">
                               <img src="images/ballon/about/s5.jpg" alt="gallary19" class="event_img" title="galary">
                    </div>
                      <div class="baby25">
                               <img src="images/ballon/about/h17.jpg" alt="gallary20" class="event_img" title="galary">
                    </div>
                      <div class="baby25">
                              <img src="images/hpimg/wel8.jpg" alt="gallary21" class="event_img" title="galary">
                    </div>
                      <div class="baby25">
                                <img src="images/ballon/about/h18.jpg" alt="gallary22" class="event_img" title="galary">
                    </div>
                     <div class="baby25">
                              <img src="images/hpimg/wel9.jpg" alt="gallary23" class="event_img" title="galary">
                    </div>
                     <div class="baby25">
                              <img src="images/hpimg/wel10.jpg" alt="gallary24" class="event_img" title="galary">
                    </div>
                     <div class="baby25">
                              <img src="images/hpimg/wel11.jpg" alt="gallary25" class="event_img" title="galary">
                    </div>-->
                </div>
            </div>
    </section>
    <!-- Footer -->
       <div w3-include-html="footer.html"></div> 
        <script>
            footerHTML();
        </script>
    <!-- toTop -->
    <a href="index.html#" class="totop">TOP</a>
    <!-- jQuery -->
    <!--<script src="js/plugins/jquery-3.5.1.min.js"></script>-->
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="assets/js/img_ajax1.js"></script>
    <script src="js/plugins/bootstrap.min.js"></script>
    <script src="js/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="js/plugins/jquery.isotope.v3.0.2.js"></script>
    <script src="js/plugins/modernizr-2.6.2.min.js"></script>
    <script src="js/plugins/jquery.waypoints.min.js"></script>
    <script src="modules/owl-carousel/owl.carousel.min.js"></script>
    <script src="modules/slick/slick.js"></script>
    <script src="modules/magnific-popup/jquery.magnific-popup.js"></script>
    <script src="modules/masonry/masonry.pkgd.min.js"></script>
    <script src="modules/youtubepopup/YouTubePopUp.js"></script>
    <script src="js/script.js"></script>
</body>

<!-- Mirrored from duruthemes.com/demo/html/bethany/demo2/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Oct 2021 05:52:04 GMT -->
</html>
<script>
    $(document).ready(function () {
            setPhotogalleryImage();
        });
</script>