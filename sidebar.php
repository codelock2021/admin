<!--<aside id="leftsidebar" class="sidebar">
     User Info 
    <div class="user-info">
        <div class="image">
            <img src="assets/images/xs/avatar7.jpg" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown">Vasu Kanani</div>
            <div class="email">vasu.codelock99@gmail.com</div>
        </div>
    </div>
     #User Info 
     Menu 
    <ul class="nav navbar-nav navbar-left">
        <li class="body">
            <ul class="menu">
                <li><a class='btn btn1 btn-outline-dark orders_info' href='multistep_image.php'>Orders</a></li>
                <li><a href="multistep_form.php"><span>Orders</span></a></li>
            </ul>
        </li>
    </ul>
</aside>-->

<aside id="leftsidebar" class="sidebar"> 
    <!-- User Info -->
    <div class="user-info">
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Happy Event</div>
            <div class="email">happyeventsurat@gmail.com</div>
        </div>
    </div>
    <!-- #User Info --> 
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="img_gallery.php" class="menu-toggle waves-effect waves-block"></i><span>Image Gallery</span></a>
            <li><a href="multistep_order.php" class="menu-toggle waves-effect waves-block"></i><span>Orders</span></a>
            <li><a href="multistep_form.php" class="menu-toggle waves-effect waves-block"></i><span>Add Orders</span></a>
            <li><a href="invoice_data.php" class="menu-toggle waves-effect waves-block"><span>Invoice</span></a>
            <li><a href="cashbook.php" class="menu-toggle waves-effect waves-block"><span>Cash Book</span></a>
            <li><a href="video_page.php" class="menu-toggle waves-effect waves-block"><span>Video Gallery</span></a>
            </li>
        </ul>
    </div>
    <!-- #Menu --> 
</aside>