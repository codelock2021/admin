<?php
$con = mysqli_connect('localhost', 'root', '', 'image_gallery');
include 'img_function.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Happy Event | Event planner | Birthday Organizer</title>
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link href="assets/plugins/waitme/waitMe.css" rel="stylesheet" />
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link  rel="stylesheet" href="assets/css/main.css">
        <link  rel="stylesheet" href="assets/css/imggallery.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="assets/js/img_ajax1.js"></script>
    </head>
    <body class="theme-orange">
        <div class="page-loader-wrapper">
            <div class="loader">        
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="100" height="100" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div><!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>
        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Image Gallery</h2>
                    </div>
                    <center><span class="error-msg clscate"></span></center>
                    <center><span class="error-msg clssize"></span></center>
                    <center><span class="error-msg fileupload1"></span></center>
                    <center><span class="error-msg clsdel"></span></center>
                    <center><span class="error-msg clsname"></span></center>
                </div>
            </div>
            <form name="iform" class="iform" enctype="multipart/form-data" >
                <div class="maindiv">
                    <input type="hidden" name="method" id="clshidden2" value="clsimage">
                    <input id="fileupload" type="file" multiple="multiple" name="image[]" style="display:none">
                    <div class="clsctgr">
                        <div class="upld">
                            <label class="clslbl">Upload Image : </label>
                            <a href="#">
                                <i id="icon_upload" class="fa fa-picture-o fa-5" onclick="upload()"></i>
                            </a>
                        </div>
                        <select class="show-tick clscategory" name="category">
                            <option value="" disabled selected hidden>Choose Category..</option>
                            <?php $query = "SELECT * from category_data order by id asc";
                                    $result = mysqli_query($con, $query);
                                     while($row = mysqli_fetch_array($result)){
                                          ?>
                            <option value="<?php echo $row['id'];?>"><?php echo $row['category'];?></option>
                                     <?php } ?>
                        </select>
                        <button class="btn btn-raised btn-success waves-effect clssubmit"><i id="clsicon" class="fa fa-spinner fa-spin clsicon" style="display: none"></i><span>Upload</span></button>
                    </div>
                    <div id="dvPreview">
                    </div>
                </div>
                <hr />
                <div class="lvpvctgry">
                     <div class="livepreview">
                    <b style="font-weight: 700;">Live Preview</b>
                    </div>
                    <div class="clsctgrys">
                        <select class="show-tick " name="clsctgry">
                            <option value="100" selected>All Category</option>
                            <?php $query = "SELECT * from category_data order by id asc";
                                    $result = mysqli_query($con, $query);
                                     while($row = mysqli_fetch_array($result)){
                                          ?>
                            <option value="<?php echo $row['id'];?>"><?php echo $row['category'];?></option>
                                     <?php } ?>
                        </select>
                    </div>
                </div>
                <center><span class="clsdata"></span></center>
                <div class="clspreview">

                </div>
            </form>
            <div class="clsnav">
                <nav class="clsnav">
                    <ul class="pagination pagination-lg pagination_html"></ul>
                </nav>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
        <script src="assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js --> 
        <!-- Bootstrap Material Datetime Picker Plugin Js --> 
        <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
        <script src="assets/js/pages/forms/basic-form-elements.js"></script> 
    </form>
</body>
</html>
<script>
        $(document).ready(function () {
            set_image();
        });
</script>